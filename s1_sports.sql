-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 01, 2016 at 04:31 AM
-- Server version: 5.6.28
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `s1_sports`
--

-- --------------------------------------------------------

--
-- Table structure for table `cometchat`
--

CREATE TABLE IF NOT EXISTS `cometchat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `sent` int(10) unsigned NOT NULL DEFAULT '0',
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `direction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `from` (`from`),
  KEY `direction` (`direction`),
  KEY `read` (`read`),
  KEY `sent` (`sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_announcements`
--

CREATE TABLE IF NOT EXISTS `cometchat_announcements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `announcement` text NOT NULL,
  `time` int(10) unsigned NOT NULL,
  `to` int(10) NOT NULL,
  `recd` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `time` (`time`),
  KEY `to_id` (`to`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_block`
--

CREATE TABLE IF NOT EXISTS `cometchat_block` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fromid` int(10) unsigned NOT NULL,
  `toid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fromid` (`fromid`),
  KEY `toid` (`toid`),
  KEY `fromid_toid` (`fromid`,`toid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cometchat_block`
--

INSERT INTO `cometchat_block` (`id`, `fromid`, `toid`) VALUES
(1, 1, 3),
(2, 1, 3),
(3, 1, 3),
(6, 1, 7),
(4, 6, 3),
(5, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_chatroommessages`
--

CREATE TABLE IF NOT EXISTS `cometchat_chatroommessages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(10) unsigned NOT NULL,
  `chatroomid` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `sent` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `chatroomid` (`chatroomid`),
  KEY `sent` (`sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_chatrooms`
--

CREATE TABLE IF NOT EXISTS `cometchat_chatrooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lastactivity` int(10) unsigned NOT NULL,
  `createdby` int(10) unsigned NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `vidsession` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lastactivity` (`lastactivity`),
  KEY `createdby` (`createdby`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `cometchat_chatrooms`
--

INSERT INTO `cometchat_chatrooms` (`id`, `name`, `lastactivity`, `createdby`, `password`, `type`, `vidsession`) VALUES
(1, 'Webappclouds', 1440498926, 10000005, '', 0, NULL),
(2, 'Ss', 1440407540, 10000011, '', 0, NULL),
(4, 'testroom', 1442579216, 10000156, '', 0, NULL),
(26, 'test app', 1445423884, 3, '', 0, NULL),
(28, 'WEB', 1442580766, 10000007, '', 0, NULL),
(33, 'CometChat_test', 1446190172, 10000708, '', 0, NULL),
(36, 'hi', 1445231713, 10000708, '', 0, NULL),
(37, 'test', 1445336703, 10000708, '', 0, NULL),
(38, 'comet', 1446190225, 10000708, '9cb5b53f83e271ec7803857b0b7803566d550330', 1, NULL),
(40, 'Test by developer', 1448367069, 10000007, '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_chatrooms_users`
--

CREATE TABLE IF NOT EXISTS `cometchat_chatrooms_users` (
  `userid` int(10) unsigned NOT NULL,
  `chatroomid` int(10) unsigned NOT NULL,
  `lastactivity` int(10) unsigned NOT NULL,
  `isbanned` int(1) DEFAULT '0',
  PRIMARY KEY (`userid`,`chatroomid`) USING BTREE,
  KEY `chatroomid` (`chatroomid`),
  KEY `lastactivity` (`lastactivity`),
  KEY `userid` (`userid`),
  KEY `userid_chatroomid` (`chatroomid`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cometchat_chatrooms_users`
--

INSERT INTO `cometchat_chatrooms_users` (`userid`, `chatroomid`, `lastactivity`, `isbanned`) VALUES
(6, 4, 1441693625, 0),
(23, 26, 1445406906, 0),
(29, 40, 1448368149, 0),
(10000002, 28, 1442560702, 0),
(10000005, 22, 1441949171, 0),
(10000007, 40, 1448369677, 0),
(10000011, 2, 1441167063, 0),
(10000023, 1, 1439990818, 0),
(10000024, 1, 1439990822, 0),
(10000025, 1, 1439991809, 0),
(10000026, 1, 1439991229, 0),
(10000030, 1, 1440000494, 0),
(10000031, 1, 1440047046, 0),
(10000032, 4, 1441632039, 0),
(10000033, 1, 1440046600, 0),
(10000034, 1, 1440045701, 0),
(10000055, 1, 1440072699, 0),
(10000276, 4, 1441633688, 0),
(10000493, 26, 1442211017, 0),
(10000596, 4, 1442233314, 0),
(10000597, 26, 1442295008, 0),
(10000599, 4, 1442295755, 0),
(10000609, 4, 1442308618, 0),
(10000619, 26, 1442411658, 0),
(10000708, 38, 1446194005, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_guests`
--

CREATE TABLE IF NOT EXISTS `cometchat_guests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `salonId` int(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastactivity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lastactivity` (`lastactivity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_messages_old_1439977069`
--

CREATE TABLE IF NOT EXISTS `cometchat_messages_old_1439977069` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `sent` int(10) unsigned NOT NULL DEFAULT '0',
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `direction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `from` (`from`),
  KEY `direction` (`direction`),
  KEY `read` (`read`),
  KEY `sent` (`sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_session`
--

CREATE TABLE IF NOT EXISTS `cometchat_session` (
  `session_id` char(32) NOT NULL,
  `session_data` text NOT NULL,
  `session_lastaccesstime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_status`
--

CREATE TABLE IF NOT EXISTS `cometchat_status` (
  `userid` int(10) unsigned NOT NULL,
  `message` text,
  `status` enum('available','away','busy','invisible','offline') DEFAULT NULL,
  `typingto` int(10) unsigned DEFAULT NULL,
  `typingtime` int(10) unsigned DEFAULT NULL,
  `isdevice` int(1) unsigned NOT NULL DEFAULT '0',
  `lastactivity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`userid`),
  KEY `typingto` (`typingto`),
  KEY `typingtime` (`typingtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cometchat_users`
--

CREATE TABLE IF NOT EXISTS `cometchat_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `displayname` varchar(100) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `grp` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `cometchat_users`
--

INSERT INTO `cometchat_users` (`id`, `username`, `password`, `displayname`, `salon_id`, `avatar`, `link`, `grp`) VALUES
(1, 'abcd', 'test', 'Test', 11, '', '', ''),
(2, 'rajeev', '1234', '', 11, '', '', ''),
(3, 'rajeev.uppala@gmail.com', 'rajeev', '', 153, 'User1.png', '', ''),
(4, 'srinu', 'srinu', '', 11, '', '', ''),
(5, 'a@gmail.com', '123456', '', 11, '', '', ''),
(6, 'Dd@yahoo.com', 'hellohello@1', '', 130, '', '', ''),
(8, 'Terrym@lordsandladiessalons.com', 'test123', '', 130, '', '', ''),
(9, 'hdreaction100@gmail.com', 'gyfl2015', '', 133, '', '', ''),
(11, 'nikhilreddy5478@gmail.com', 'delete', '', 1, '', '', ''),
(12, 'Terencemckim@yahoo.com', 'addy150', '', 92, '', '', ''),
(13, 'emaila@email.com', 'passworda', '', 1, '', '', ''),
(14, 'admin@mikethepoet.com', 'admin@123', '', 1, '', '', ''),
(15, 'nithinredd@gmail.com', 'password', '', 1, 'User1.png', '', ''),
(16, 'howrobjr@aol.com ', 'Jus4u2nv!', '', 134, '', '', ''),
(17, 'srinivasulu.1216@gmail.com', '1234', '', 1, 'User1.png', '', ''),
(18, 'test@gmail.com', '1234567', '', 1, 'User1.png', '', ''),
(19, 'amankaler0@gmail.com', 'test1234', '', 169, '', '', ''),
(20, 'silvajasmineg@yahoo.com', 'sportyfox', '', 137, '', '', ''),
(21, 'st@gmail.com', 'st', '', 140, '', '', ''),
(22, 'bc.outsiderz@gmail.com', 'dan2121315', '', 141, '', '', ''),
(23, 's@gmail.com', 's', '', 142, '', '', ''),
(24, 'Danemichael.miller@gmail.com', 'hurricane', '', 145, '', '', ''),
(25, 'Dane@garagestrength.com', 'hurricane', '', 147, '', '', ''),
(27, 'rajeev@webappclouds.com', '1234', '', 154, '', '', ''),
(28, 'Test@tester.com', 'test', '', 152, '', '', ''),
(29, 'kranthi@webappclouds.com', 'kranthi123', '', 153, '', '', ''),
(31, 'kranthiethan@gmail.com', 'asdfgh', '', 158, '', '', ''),
(32, 'Dilan@webappclouds.com', 'ibsonibson', '', 154, '', '', ''),
(33, 'coachd375@gmail.com ', 'Phillycop2519', '', 159, '', '', ''),
(34, 'bugginhome@aol.com ', 'vhopper27', '', 164, '', '', ''),
(35, 'Corneliusgeorge04@gmail.com', 'football30', '', 164, '', '', ''),
(36, 'hunter@gmail.com', 'asdfgh', '', 157, '', '', ''),
(37, 'Prakash@webappclouds.com', 'prakash', '', 153, '', '', ''),
(38, 'Ken.thomason67@gmail.com', 'bulldogs67', '', 165, '', '', ''),
(39, 'aman@webappclouds.com', 'test1234', '', 166, '', '', ''),
(40, 'davejones@premierrents.com ', 'premier001', '', 168, '', '', ''),
(41, 'usskc@yahoo.com ', 'jones', '', 168, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `plus_background_images`
--

CREATE TABLE IF NOT EXISTS `plus_background_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `image_name` varchar(200) NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=107 ;

--
-- Dumping data for table `plus_background_images`
--

INSERT INTO `plus_background_images` (`image_id`, `salon_id`, `image_name`) VALUES
(106, 1, 'background_53d23dfddca91.png');

-- --------------------------------------------------------

--
-- Table structure for table `plus_calendar`
--

CREATE TABLE IF NOT EXISTS `plus_calendar` (
  `calendar_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `calendar_title` varchar(200) NOT NULL,
  `calendar_desc` text NOT NULL,
  `calendar_date` varchar(50) NOT NULL,
  `calendar_address` text NOT NULL,
  `calendar_lat` varchar(100) NOT NULL,
  `calendar_lon` varchar(100) NOT NULL,
  `calendar_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `calendar_create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `calendar_update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`calendar_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `plus_calendar`
--

INSERT INTO `plus_calendar` (`calendar_id`, `module_id`, `salon_id`, `calendar_title`, `calendar_desc`, `calendar_date`, `calendar_address`, `calendar_lat`, `calendar_lon`, `calendar_status`, `calendar_create_date`, `calendar_update_date`) VALUES
(21, 383, 1, 'testing event', 'testing', '09/02/2014', 'cqqqqqqqqqqqqqqqq', '0', '0', 'Active', '2014-08-13 03:47:52', '2014-08-31 13:53:27'),
(19, 383, 1, 'test event', 'test event', '08/04/2014', 'chandigarh', '0', '0', 'Active', '2014-08-08 06:45:29', '2014-08-08 06:45:29'),
(20, 383, 1, 'Test', 'Descr', '08/28/2014', 'Addr', '0', '0', 'Active', '2014-08-11 04:44:49', '2014-08-11 04:44:49'),
(16, 383, 1, 'hello', 'hello', '08/07/2014', 'manali hp', '0', '0', 'Active', '2014-08-07 10:13:35', '2014-08-08 01:53:50'),
(18, 383, 1, 'hello', 'thanks', '08/22/2014', 'sdgsdgsdg@gmail.com', '0', '0', 'Active', '2014-08-08 05:29:35', '2014-08-08 05:29:35'),
(14, 383, 1, 'event55', 'event 55', '08/08/2014', '#169, ph', '0', '0', 'Active', '2014-08-07 00:51:03', '2014-08-07 00:51:03'),
(15, 426, 13, 'sdgsdgg', 'test', '08/07/2014', 'sdgsdgsdg@gmail.com', '0', '0', 'Active', '2014-08-07 06:41:57', '2014-08-07 06:41:57'),
(10, 408, 11, 'Test', 'Test Desc', '07/08/2015 11:11', 'Addr', '0', '0', 'Active', '2014-07-15 09:33:21', '2015-03-10 03:02:21'),
(12, 383, 1, 'team test', 'team', '07/31/2014', '219 winter wood', '0', '0', 'Inactive', '2014-07-31 08:46:32', '2014-08-07 10:20:54'),
(23, 383, 1, 'new test event', 'testing', '09/22/2014', 'test', '0', '0', 'Active', '2014-09-22 08:43:51', '2014-09-22 08:43:51'),
(28, 383, 1, 'Tmrw test', 'Description test', '12/19/2014', 'Addr', '0', '0', 'Active', '2014-12-16 08:20:00', '2014-12-16 08:21:27'),
(29, 435, 14, 'Testevent here', 'Descr', '12/18/2014', 'Addr', '0', '0', 'Active', '2014-12-18 09:00:50', '2014-12-18 09:00:50'),
(32, 498, 49, 'practice', 'wspecial teams', '03/10/2015', '335 stoop ave', '0', '0', 'Active', '2015-03-05 10:52:40', '2015-03-05 10:52:40'),
(31, 498, 49, 'walk thru', 'go over this weeks game plan', '03/09/2015', 'highschool', '0', '0', 'Active', '2015-03-05 07:42:55', '2015-03-05 07:42:55'),
(33, 408, 11, 'test', 'asfasf', '03/25/2015 16:30', 'test', '0', '0', 'Active', '2015-03-10 02:43:00', '2015-08-08 08:50:22'),
(34, 534, 67, 'test', 'test', '03/11/2015 18:17', 'pankaj@gmail.com', '0', '0', 'Active', '2015-03-10 07:02:34', '2015-03-10 07:02:52'),
(36, 541, 77, 'test', 'hello', '03/25/2015 03:07', 'pankaj@gmail.com', '0', '0', 'Active', '2015-03-25 05:52:08', '2015-03-25 05:52:23'),
(37, 408, 11, 'test calendar webservice', 'bla bla bla bla', '03/25/2015 16:30', 'najaj aja j aja ja ja ', '0', '0', 'Active', '2015-08-08 08:52:02', '2015-08-08 08:52:02'),
(38, 408, 11, 'test', 'test', '08-09-2015 19:32', 'test', '0', '0', 'Active', '2015-08-08 10:04:28', '2015-08-08 10:04:28'),
(39, 408, 11, 'test1', 'test1', '08-09-2015 19:42', 'test1', '0', '0', 'Active', '2015-08-08 10:12:31', '2015-08-08 10:12:31'),
(40, 408, 11, 'test3', 'test3', '08-11-2015 10:45', 'test3', '0', '0', 'Active', '2015-08-08 10:20:55', '2015-08-08 10:20:55'),
(41, 408, 11, 'test 4', 'test4', '08-11-2015 19:50', 'test4', '0', '0', 'Active', '2015-08-08 10:22:50', '2015-08-08 10:22:50'),
(42, 408, 11, 'web', 'web', '08-12-2015 20:05', 'web', '0', '0', 'Active', '2015-08-08 10:38:05', '2015-08-08 10:38:05'),
(43, 408, 11, 'web2', 'web2', '08-13-2015 22:10', 'web2', '0', '0', 'Active', '2015-08-08 10:45:11', '2015-08-08 10:45:11'),
(44, 408, 11, 'web3', 'desc', '08-14-2015 20:30', 'address', '0', '0', 'Active', '2015-08-08 11:04:22', '2015-08-08 11:04:22'),
(45, 408, 11, 'Web11', 'web22', '08-09-2015 20:34', 'Test Web', '0', '0', 'Active', '2015-08-08 11:05:01', '2015-08-08 11:05:01'),
(46, 408, 11, 'web4', 'desc', '08-16-2015 21:00', 'addree', '0', '0', 'Active', '2015-08-08 11:32:05', '2015-08-08 11:32:05'),
(47, 408, 11, 'web5', 'desce', '08-20-2015 21:00', 'aad', '0', '0', 'Active', '2015-08-08 11:32:55', '2015-08-08 11:32:55'),
(48, 408, 11, 'web6', 'web6', '08-18-2015 21:10', 'web6', '0', '0', 'Active', '2015-08-08 11:41:46', '2015-08-08 11:41:46'),
(49, 408, 11, 'Dr', 'test', '08/10/2015 11:21', 'dr', '0', '0', 'Active', '2015-08-10 01:52:15', '2015-08-10 01:52:15'),
(50, 902, 130, 'test', 'test by developer', '08-19-2015 23:38', 'test', '0', '0', 'Active', '2015-08-18 14:08:32', '2015-08-18 14:08:32'),
(51, 498, 49, 'Game', 'Home game', '09-25-2015 19:00', '1111 Shoe   Dr', '0', '0', 'Active', '2015-09-07 23:59:56', '2015-09-07 23:59:56'),
(52, 498, 49, 'Game', 'Away', '09-09-2015 07:24', '71 Stoop Way', '0', '0', 'Active', '2015-09-08 07:25:34', '2015-09-08 07:25:34'),
(53, 408, 11, 'Test By Developer', 'Test', '10-19-2015 15:39', 'Test', '0', '0', 'Active', '2015-10-18 06:10:32', '2015-10-18 06:10:32'),
(54, 408, 11, 'test', 'test', '10-20-2015 12:14', 'test', '0', '0', 'Active', '2015-10-19 02:46:08', '2015-10-19 02:46:08'),
(55, 408, 11, 'title2', 'title2', '10-20-2015 12:16', 'title2', '0', '0', 'Active', '2015-10-19 02:47:14', '2015-10-19 02:47:14'),
(56, 408, 11, 'title3', 'title3', '10-21-2015 12:15', 'title3', '0', '0', 'Active', '2015-10-19 02:48:42', '2015-10-19 02:48:42'),
(57, 408, 11, 'Jfkfi', 'Fjgktdkfk', '10-20-2015 12:21', 'Flag', '0', '0', 'Active', '2015-10-19 02:51:32', '2015-10-19 02:51:32'),
(58, 408, 11, 'H', 'Hu', '10-24-2015 12:25', 'I', '0', '0', 'Active', '2015-10-19 02:53:36', '2015-10-19 02:53:36'),
(59, 408, 11, 'test by developer', 'test', '10-23-2015 12:50', 'test', '0', '0', 'Active', '2015-10-19 03:24:02', '2015-10-19 03:24:02'),
(60, 498, 49, 'Game', 'JV  game ', '10-21-2015 13:48', '2221 Shitsville Rd', '0', '0', 'Active', '2015-10-20 13:49:40', '2015-10-20 13:49:40'),
(61, 498, 49, 'Game', 'Varsity game', '10-21-2015 16:49', '304 Asfffgh', '0', '0', 'Active', '2015-10-20 16:49:55', '2015-10-20 16:49:55'),
(62, 986, 142, 'test', 'test', '10-22-2015 11:13', 'test', '0', '0', 'Active', '2015-10-21 01:43:39', '2015-10-21 01:43:39'),
(63, 408, 11, 'test', 'abc', '10/23/2015 3:33', 'abc', '0', '0', 'Active', '2015-10-21 06:03:32', '2015-10-21 06:03:32'),
(64, 408, 11, 'test', 'test', '10/25/2015 4:1', 'test', '0', '0', 'Active', '2015-10-21 06:34:55', '2015-10-21 06:34:55'),
(65, 1070, 154, 'Test', 'Test', '10-30-2015 04:42', '215', '0', '0', 'Active', '2015-10-29 05:43:08', '2015-10-29 05:43:08'),
(66, 1063, 153, 'test by developer', 'test ', '11/17/2015', 'test by developer ', '0', '0', 'Active', '2015-11-17 06:02:41', '2015-11-17 06:02:41'),
(67, 1140, 164, 'Game', 'JV game ', '11-25-2015 16:30', '300 Pearl Ed', '0', '0', 'Active', '2015-11-17 09:37:17', '2015-11-17 09:37:17'),
(68, 1154, 166, 'test', 'test app', '11-30-2015 19:00', 'abc', '0', '0', 'Active', '2015-11-25 00:27:44', '2015-11-25 00:27:44'),
(69, 1098, 158, 'test by dev', 'test', '02-26-2016 14:52', 'test', '0', '0', 'Active', '2016-02-25 04:22:46', '2016-02-25 04:22:46'),
(70, 1070, 154, 'test by dev', 'test', '03-02-2016 12:13', 'test', '0', '0', 'Active', '2016-03-01 01:43:40', '2016-03-01 01:43:40');

-- --------------------------------------------------------

--
-- Table structure for table `plus_config`
--

CREATE TABLE IF NOT EXISTS `plus_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `config_aboutus` text NOT NULL,
  `config_contactus` text NOT NULL,
  `config_phone` varchar(15) NOT NULL,
  `config_emailid` varchar(100) NOT NULL,
  `config_hours` text NOT NULL,
  `config_texting_message` varchar(100) NOT NULL,
  `glamour_images_tnc` text NOT NULL,
  `referral_faq` text NOT NULL,
  `config_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `config_create_date` datetime NOT NULL,
  `config_update_date` datetime NOT NULL,
  `banner_image` varchar(100) NOT NULL DEFAULT 'no_salon_image.gif',
  `cpay_apikey` varchar(100) NOT NULL,
  `cpay_token` varchar(150) NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

--
-- Dumping data for table `plus_config`
--

INSERT INTO `plus_config` (`config_id`, `salon_id`, `config_aboutus`, `config_contactus`, `config_phone`, `config_emailid`, `config_hours`, `config_texting_message`, `glamour_images_tnc`, `referral_faq`, `config_status`, `config_create_date`, `config_update_date`, `banner_image`, `cpay_apikey`, `cpay_token`) VALUES
(93, 92, '', '', '', '', '', '', '', '', 'Active', '2015-03-30 14:12:19', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(94, 94, '', '', '', '', '', '', '', '', 'Active', '2015-03-31 12:36:49', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(96, 93, '', '', '', '', '', '', '', '', 'Active', '2015-04-01 06:54:10', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(100, 102, '', '', '', '', '', '', '', '', 'Active', '2015-04-02 06:22:25', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(103, 115, '', '', '', '', '', '', '', '', 'Active', '2015-06-19 17:23:50', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(104, 130, '', '', '', '', '', '', '', '', 'Active', '2015-08-18 13:23:35', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(105, 143, '', '', '', '', '', '', '', '', 'Active', '2015-10-27 20:14:33', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(106, 141, '', '', '', '', '', '', '', '', 'Active', '2015-10-28 06:38:11', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(107, 148, '', '', '', '', '', '', '', '', 'Active', '2015-10-28 06:57:01', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(108, 153, '', '', '', '', '', '', '', '', 'Active', '2015-11-17 07:30:43', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(109, 168, '', '', '', '', '', '', '', '', 'Active', '2015-12-28 12:44:48', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(110, 170, '', '', '', '', '', '', '', '', 'Active', '2016-02-03 15:54:54', '0000-00-00 00:00:00', 'no_salon_image.gif', '', ''),
(111, 154, '', '', '', '', '', '', '', '', 'Active', '2016-03-01 01:48:21', '0000-00-00 00:00:00', 'no_salon_image.gif', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `plus_contacts`
--

CREATE TABLE IF NOT EXISTS `plus_contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(2) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `message` text NOT NULL,
  `type` varchar(200) NOT NULL,
  `added_on` varchar(200) NOT NULL,
  KEY `contact_id` (`contact_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=236 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_contact_us`
--

CREATE TABLE IF NOT EXISTS `plus_contact_us` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `added_on` varchar(200) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_events`
--

CREATE TABLE IF NOT EXISTS `plus_events` (
  `events_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `events_name` varchar(200) NOT NULL,
  `events_from_date` varchar(200) NOT NULL,
  `events_to_date` varchar(200) NOT NULL,
  `events_description` text NOT NULL,
  `events_status` varchar(10) NOT NULL,
  `events_create_date` datetime NOT NULL,
  `events_update_date` datetime NOT NULL,
  `events_order` int(11) NOT NULL DEFAULT '1',
  `unique_code` varchar(30) NOT NULL,
  PRIMARY KEY (`events_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=99 ;

--
-- Dumping data for table `plus_events`
--

INSERT INTO `plus_events` (`events_id`, `module_id`, `salon_id`, `events_name`, `events_from_date`, `events_to_date`, `events_description`, `events_status`, `events_create_date`, `events_update_date`, `events_order`, `unique_code`) VALUES
(55, 385, 1, 'Event Title', '04/24/2014', '2014-07-14', 'Descr', 'Active', '2014-04-28 06:46:56', '2014-07-14 07:37:32', 2, '9DDHSW035Q'),
(57, 385, 1, 'test-event', '07/27/2014', '2014-07-31', 'Sports Event', 'Active', '2014-07-10 10:28:35', '2014-07-31 08:30:35', 1, '4DD8CS035AG'),
(67, 429, 13, 'test', 'test', '2014-10-10', 'test', 'Active', '2014-10-10 07:19:47', '2014-10-10 07:19:47', 1, '63330EF044'),
(65, 385, 1, 'hello test', '10/10/2014', '2014-08-07', 'vzxv', 'Inactive', '2014-08-07 10:12:29', '2014-08-07 10:12:29', 1, '735E038B4A'),
(93, 1152, 166, 'test', '2015-11-25', '2015-11-25', 'app', 'Active', '2015-11-25 00:29:48', '2015-11-25 00:29:48', 1, '12ECA1A11A'),
(68, 385, 1, 'Test', 'Test', '2014-12-16', 'Desc', 'Active', '2014-12-16 02:20:53', '2014-12-16 02:20:53', 1, '7D3A541C42'),
(69, 438, 14, 'Test', 'Mar 24, 2014', '2014-12-18', 'Descr', 'Active', '2014-12-18 09:00:05', '2014-12-18 09:00:05', 1, '06AEC03454'),
(70, 500, 49, 'chicken bbq', '03/19/15', '2015-03-04', 'bbq chicken sale', 'Active', '2015-03-04 13:37:36', '2015-03-04 13:37:36', 1, '66CA0BA95C'),
(84, 900, 130, 'event', '2015-08-19', '2015-08-18', 'event', 'Active', '2015-08-18 14:07:54', '2015-08-18 14:07:54', 1, '32B33C57E8'),
(92, 1152, 166, 'test', '2015-11-25', '2015-11-25', 'app', 'Active', '2015-11-25 00:29:33', '2015-11-25 00:29:33', 1, '9E4D010DBC'),
(91, 411, 11, 'testA', '2015-10-22', '2015-10-21', 'test', 'Active', '2015-10-21 06:40:19', '2015-10-21 06:40:19', 1, 'D303631C1D'),
(90, 984, 142, 'test', '2015-10-21', '2015-10-21', 'test', 'Active', '2015-10-21 01:44:24', '2015-10-21 01:44:24', 1, '7778448F3C'),
(89, 411, 11, 'test3', '2015-10-21', '2015-10-19', 'test', 'Active', '2015-10-19 03:21:56', '2015-10-19 03:21:56', 1, '430E01298C'),
(88, 411, 11, 'test2', '2015-10-20', '2015-10-19', 'test', 'Active', '2015-10-19 03:21:18', '2015-10-19 03:21:18', 1, 'EEB9060EAF'),
(87, 411, 11, 'Test ', '2015-10-19', '2015-10-19', 'Test desc', 'Active', '2015-10-19 02:24:42', '2015-10-19 02:24:42', 1, '8565575DCA'),
(85, 907, 131, 'test', '2015-09-11', '2015-09-11', 'test', 'Active', '2015-09-11 03:29:13', '2015-09-11 03:29:13', 1, '36A4BB10CA'),
(86, 509, 50, 'Game 1', '2015-09-24', '2015-09-18', 'Soccer ', 'Active', '2015-09-18 20:57:41', '2015-09-18 20:57:41', 1, '9CCCE6DE53'),
(94, 1152, 166, 'test26', '2015-11-26', '2015-11-25', 'abc', 'Active', '2015-11-25 00:30:34', '2015-11-25 00:30:34', 1, 'B83F967262'),
(95, 1180, 170, 'Test', '2/3/2016', '2016-02-03', 'Tester', 'Active', '2016-02-03 15:53:57', '2016-02-03 15:53:57', 1, '63E1E69D5D'),
(96, 1096, 158, 'test event', '2016-02-25', '2016-02-25', 'tet', 'Active', '2016-02-25 04:23:46', '2016-02-25 04:23:46', 1, '327D42249A'),
(97, 1068, 154, 'Test  Event', '2016-03-01', '2016-03-01', 'test', 'Active', '2016-03-01 01:45:53', '2016-03-01 01:45:53', 1, '825735EA2B');

-- --------------------------------------------------------

--
-- Table structure for table `plus_event_checkins`
--

CREATE TABLE IF NOT EXISTS `plus_event_checkins` (
  `checkin_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `checkin` datetime NOT NULL,
  PRIMARY KEY (`checkin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `plus_event_checkins`
--

INSERT INTO `plus_event_checkins` (`checkin_id`, `event_id`, `salon_id`, `module_id`, `user_email`, `checkin`) VALUES
(6, 57, 1, 385, 'test1@test.com', '2014-07-27 11:45:43'),
(3, 57, 1, 385, 'rajeev070291@gmail.com', '2014-07-16 02:05:18'),
(4, 56, 1, 385, 'rajeev070291@gmail.com', '2014-07-16 02:05:28'),
(7, 62, 11, 411, 'rajeev.uppala@gmail.com', '2015-03-16 02:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `plus_event_registrations`
--

CREATE TABLE IF NOT EXISTS `plus_event_registrations` (
  `event_register_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `register_on` datetime NOT NULL,
  PRIMARY KEY (`event_register_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `plus_event_registrations`
--

INSERT INTO `plus_event_registrations` (`event_register_id`, `event_id`, `salon_id`, `module_id`, `user_email`, `register_on`) VALUES
(1, 56, 1, 386, 'rajeev.uppala@gmail.com', '2014-07-15 08:47:11'),
(16, 57, 1, 385, 'rajeev070291@gmail.com', '2014-07-16 06:16:55'),
(17, 57, 1, 385, 'test1@test.com', '2014-07-27 11:31:45'),
(4, 55, 1, 385, 'nithinredd@gmail.com', '2014-07-15 09:26:09'),
(5, 55, 1, 385, 'nithinredd@gmail.com', '2014-07-15 09:26:54'),
(6, 55, 1, 385, 'nithinredd@gmail.com', '2014-07-15 09:27:18'),
(13, 57, 1, 385, 'rajeev.uppala@gmail.com', '2014-07-16 02:04:36'),
(18, 55, 1, 385, 'test1@test.com', '2014-07-31 09:31:36'),
(19, 55, 1, 385, 'hamburgcoachneil@aol.com', '2014-08-14 11:05:51'),
(20, 70, 49, 500, 'hamburgcoachneil@aol.com', '2015-03-04 13:39:01'),
(21, 62, 11, 411, 'rajeev.uppala@gmail.com', '2015-03-16 02:15:07'),
(22, 57, 1, 385, 'hamburgcoachneil@aol.com', '2015-08-02 09:30:01'),
(23, 81, 11, 411, 'rajeev.uppala@gmail.com', '2015-08-08 12:10:24'),
(24, 83, 11, 411, 'rajeev.uppala@gmail.com', '2015-08-18 02:44:06'),
(25, 90, 142, 984, 's@gmail.com', '2015-10-21 01:44:34'),
(26, 94, 166, 1152, 'aman@webappclouds.com', '2015-11-25 00:31:12'),
(27, 96, 158, 1096, 'kranthiethan@gmail.com', '2016-02-25 04:26:24'),
(28, 97, 154, 1068, 'Dilan@webappclouds.com', '2016-03-01 01:46:03');

-- --------------------------------------------------------

--
-- Table structure for table `plus_feedback`
--

CREATE TABLE IF NOT EXISTS `plus_feedback` (
  `feedback_id` int(20) NOT NULL AUTO_INCREMENT,
  `salon_id` int(20) NOT NULL,
  `feedback_date` varchar(200) NOT NULL,
  `feedback_name` varchar(200) NOT NULL,
  `feedback_email` varchar(200) NOT NULL,
  `feedback_title` text NOT NULL,
  `feedback_description` text NOT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_gallery`
--

CREATE TABLE IF NOT EXISTS `plus_gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `gallery_name` varchar(100) NOT NULL,
  `gallery_title` varchar(100) NOT NULL,
  `gallery_image` varchar(255) NOT NULL,
  `gallery_description` text NOT NULL,
  `gallery_status` enum('Active','Inactive') NOT NULL,
  `gallery_create_date` datetime NOT NULL,
  `gallery_update_date` datetime NOT NULL,
  `gallery_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1224 ;

--
-- Dumping data for table `plus_gallery`
--

INSERT INTO `plus_gallery` (`gallery_id`, `module_id`, `salon_id`, `gallery_name`, `gallery_title`, `gallery_image`, `gallery_description`, `gallery_status`, `gallery_create_date`, `gallery_update_date`, `gallery_order`) VALUES
(1217, 899, 130, '0', 'test ', 'gallery_5975884101439921243.jpeg', 'test by developer', 'Active', '2015-08-18 14:07:23', '2015-08-18 14:07:23', 1),
(1218, 1151, 166, '0', 'test', 'gallery_5246654171448430269.jpeg', 'test app', 'Active', '2015-11-25 00:44:29', '2015-11-25 00:44:29', 1),
(1219, 1060, 153, '0', 'Test', 'gallery_17280048551448431252.jpeg', 'Test', 'Active', '2015-11-25 01:00:53', '2015-11-25 01:00:53', 1),
(1220, 1060, 153, '0', 'Test By Developer', 'gallery_11644218971448520533.jpeg', 'Test', 'Active', '2015-11-26 01:48:53', '2015-11-26 01:48:53', 1),
(1221, 1095, 158, '0', 'test image', 'gallery_17800436101456392108.jpeg', 'test', 'Active', '2016-02-25 04:21:48', '2016-02-25 04:21:48', 1),
(1222, 1095, 158, '0', 'test', 'gallery_10122179151456397023.jpeg', 'test', 'Active', '2016-02-25 05:43:43', '2016-02-25 05:43:43', 1),
(1223, 1067, 154, '0', 'test', 'gallery_20802415081456815295.jpeg', 'test', 'Active', '2016-03-01 01:54:55', '2016-03-01 01:54:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `plus_groups`
--

CREATE TABLE IF NOT EXISTS `plus_groups` (
  `groups_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `groups_title` varchar(100) NOT NULL,
  `groups_status` enum('Active','Inactive') NOT NULL,
  `groups_create_date` datetime NOT NULL,
  `groups_update_date` datetime NOT NULL,
  `groups_order` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`groups_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=113 ;

--
-- Dumping data for table `plus_groups`
--

INSERT INTO `plus_groups` (`groups_id`, `module_id`, `salon_id`, `groups_title`, `groups_status`, `groups_create_date`, `groups_update_date`, `groups_order`) VALUES
(1, 381, 1, 'First One', 'Inactive', '2014-04-27 00:00:00', '2014-04-27 05:28:49', 2),
(9, 381, 1, 'Group11', 'Active', '2014-08-07 00:45:42', '2014-08-07 00:45:49', 1),
(3, 381, 1, 'test', 'Active', '2014-06-19 09:05:09', '2014-06-19 09:05:09', 1),
(5, 406, 11, 'Team webappclouds', 'Active', '2014-08-01 05:29:18', '2015-07-15 02:48:44', 1),
(7, 381, 1, 'test group', 'Inactive', '2014-08-06 05:00:30', '2014-08-06 05:00:30', 1),
(10, 381, 1, 'Php Group', 'Active', '2014-08-08 01:48:08', '2014-08-08 01:48:48', 1),
(11, 442, 15, 'test', 'Active', '2014-10-10 12:47:59', '2014-10-10 12:47:59', 1),
(12, 533, 67, 'test', 'Active', '2015-03-10 07:01:42', '2015-03-10 07:01:42', 1),
(13, 533, 67, 'test demo', 'Active', '2015-03-10 07:20:18', '2015-03-10 07:21:04', 1),
(14, 543, 77, 'test', 'Active', '2015-03-25 05:57:23', '2015-03-25 05:57:23', 1),
(15, 590, 92, 'test', 'Active', '2015-04-01 00:29:32', '2015-04-01 00:29:32', 1),
(16, 690, 103, 'test', 'Active', '2015-04-02 05:13:33', '2015-04-02 05:13:33', 1),
(17, 720, 107, 'midgets', 'Inactive', '2015-04-02 12:31:20', '2015-04-02 12:34:01', 1),
(84, 774, 113, 'Saints', 'Active', '2015-07-28 04:40:00', '2015-07-28 04:40:00', 1),
(96, 898, 130, 'Team', 'Active', '2015-08-18 14:59:12', '2015-08-18 14:59:12', 1),
(95, 898, 130, 'Web', 'Active', '2015-08-18 14:05:10', '2015-08-18 14:05:10', 1),
(94, 496, 49, 'Joe', 'Active', '2015-08-15 08:47:23', '2015-08-15 08:47:23', 1),
(93, 496, 49, 'Midgets', 'Active', '2015-08-15 08:47:03', '2015-08-15 08:47:03', 1),
(92, 406, 11, 'Webapp', 'Active', '2015-08-04 07:34:10', '2015-08-10 05:24:58', 1),
(97, 505, 50, 'Team', 'Active', '2015-09-18 20:55:01', '2015-09-18 20:55:01', 1),
(98, 406, 11, 'Team', 'Active', '2015-10-19 02:59:38', '2015-10-19 02:59:38', 1),
(99, 406, 11, 'Team1', 'Active', '2015-10-19 02:59:53', '2015-10-19 02:59:53', 1),
(100, 406, 11, 'Team3', 'Active', '2015-10-19 03:02:11', '2015-10-19 03:02:11', 1),
(101, 406, 11, 'Team4', 'Active', '2015-10-19 03:18:51', '2015-10-19 03:18:51', 1),
(102, 406, 11, 'TEST', 'Active', '2015-10-20 01:25:06', '2015-10-20 01:25:06', 1),
(103, 968, 140, 'Test', 'Active', '2015-10-21 00:57:28', '2015-10-21 00:57:28', 1),
(104, 982, 142, 'Test', 'Active', '2015-10-21 01:45:10', '2015-10-21 01:45:10', 1),
(105, 496, 49, 'Mites', 'Active', '2015-10-22 13:16:18', '2015-10-22 13:16:18', 1),
(106, 1059, 153, 'Test', 'Active', '2015-10-29 07:47:40', '2015-10-29 07:47:40', 1),
(107, 1143, 165, 'Coaches', 'Active', '2015-11-17 17:43:42', '2015-11-17 17:43:42', 1),
(108, 1150, 166, 'Test App', 'Active', '2015-11-25 00:37:09', '2015-11-25 00:37:09', 1),
(109, 1150, 166, 'Test App', 'Active', '2015-11-25 00:37:28', '2015-11-25 00:37:28', 1),
(110, 1164, 168, 'SoS Elite (Seven on Seven)', 'Active', '2015-12-23 00:25:13', '2016-01-04 16:03:43', 1),
(111, 1094, 158, 'test group', 'Active', '2016-02-25 04:27:17', '2016-02-25 04:27:17', 1),
(112, 1066, 154, 'Test Group', 'Active', '2016-03-01 01:46:34', '2016-03-01 01:46:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `plus_groups_members`
--

CREATE TABLE IF NOT EXISTS `plus_groups_members` (
  `groups_members_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `groups_id` int(11) NOT NULL,
  `team_members_id` int(11) NOT NULL,
  PRIMARY KEY (`groups_members_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=220 ;

--
-- Dumping data for table `plus_groups_members`
--

INSERT INTO `plus_groups_members` (`groups_members_id`, `module_id`, `salon_id`, `groups_id`, `team_members_id`) VALUES
(1, 381, 1, 1, 2),
(45, 381, 1, 9, 16),
(3, 381, 1, 1, 4),
(12, 381, 1, 3, 8),
(9, 381, 1, 3, 2),
(41, 381, 1, 10, 71),
(11, 381, 1, 3, 6),
(27, 381, 1, 9, 68),
(43, 381, 1, 10, 66),
(44, 381, 1, 9, 71),
(17, 381, 1, 3, 14),
(18, 381, 1, 3, 11),
(19, 406, 11, 5, 10),
(26, 381, 1, 9, 69),
(40, 381, 1, 10, 70),
(39, 381, 1, 10, 74),
(38, 381, 1, 9, 70),
(46, 381, 1, 9, 14),
(47, 442, 15, 11, 124),
(49, 406, 11, 5, 117),
(167, 898, 130, 95, 215),
(118, 406, 11, 5, 188),
(166, 406, 11, 5, 211),
(143, 774, 113, 84, 203),
(168, 898, 130, 95, 214),
(147, 406, 11, 5, 0),
(165, 406, 11, 5, 0),
(164, 406, 11, 92, 211),
(161, 406, 11, 92, 0),
(159, 406, 11, 92, 117),
(158, 406, 11, 92, 0),
(169, 505, 50, 97, 218),
(170, 982, 142, 104, 224),
(171, 406, 11, 5, 225),
(172, 496, 49, 105, 127),
(173, 496, 49, 105, 126),
(174, 496, 49, 105, 125),
(175, 496, 49, 105, 128),
(176, 496, 49, 94, 125),
(177, 496, 49, 94, 126),
(178, 496, 49, 94, 127),
(179, 496, 49, 94, 128),
(180, 1059, 153, 106, 232),
(181, 1150, 166, 108, 243),
(182, 1150, 166, 108, 244),
(183, 1164, 168, 110, 0),
(184, 1164, 168, 110, 245),
(185, 1164, 168, 110, 246),
(186, 1164, 168, 110, 247),
(187, 1164, 168, 110, 248),
(188, 1164, 168, 110, 250),
(189, 1164, 168, 110, 251),
(190, 1164, 168, 110, 252),
(191, 1164, 168, 110, 253),
(192, 1164, 168, 110, 254),
(193, 1164, 168, 110, 255),
(194, 1164, 168, 110, 256),
(195, 1164, 168, 110, 257),
(196, 1094, 158, 111, 258),
(197, 1094, 158, 111, 259),
(199, 1094, 158, 111, 261),
(201, 1066, 154, 112, 0),
(202, 1066, 154, 112, 0),
(213, 1066, 154, 112, 0),
(204, 1066, 154, 112, 0),
(205, 1066, 154, 112, 0),
(206, 1066, 154, 112, 0),
(207, 1066, 154, 112, 0),
(208, 1066, 154, 112, 268),
(209, 1066, 154, 112, 0),
(214, 1066, 154, 112, 0),
(211, 1066, 154, 112, 0),
(212, 1066, 154, 112, 230),
(215, 1066, 154, 112, 265),
(216, 1066, 154, 112, 0),
(217, 1066, 154, 112, 273),
(218, 1066, 154, 112, 0),
(219, 1066, 154, 112, 275);

-- --------------------------------------------------------

--
-- Table structure for table `plus_messages`
--

CREATE TABLE IF NOT EXISTS `plus_messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `timestamp` datetime NOT NULL,
  `send_or_receive` enum('1','2') NOT NULL,
  `shown` enum('0','1') NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=338 ;

--
-- Dumping data for table `plus_messages`
--

INSERT INTO `plus_messages` (`message_id`, `salon_id`, `email`, `message`, `timestamp`, `send_or_receive`, `shown`) VALUES
(26, 1, 'shekar', 'testing', '2014-06-11 06:17:37', '1', '1'),
(27, 1, 'shekar', 'testing this', '2014-06-11 06:17:54', '1', '1'),
(28, 1, 'shekar', 'testing this from mobile', '2014-06-11 06:21:42', '1', '1'),
(2, 1, 'rajeev', 'hi testing this again..', '2014-06-10 00:53:35', '1', '1'),
(4, 1, 'nithin', 'testing by nithin', '2014-06-10 04:05:19', '1', '1'),
(5, 1, 'Admin', 'testing from admin', '2014-06-11 03:05:54', '2', '1'),
(14, 1, 'Admin', 'rajeev testing it...', '2014-06-11 04:33:52', '2', '1'),
(23, 1, 'Admin', 'final test...', '2014-06-11 06:01:02', '2', '1'),
(29, 1, 'Admin', 'hi', '2014-06-11 06:25:05', '2', '1'),
(30, 1, 'shekar', 'hi', '2014-06-11 06:25:42', '1', '1'),
(31, 1, 'shekar', 'hi again', '2014-06-11 06:26:50', '1', '1'),
(32, 1, 'Admin', 'ya', '2014-06-11 06:26:59', '2', '1'),
(33, 1, 'Admin', 'test', '2014-06-11 06:27:03', '2', '1'),
(34, 1, 'shekar', 'hi again from rajeev', '2014-06-11 06:34:17', '1', '1'),
(35, 1, 'shekar', 'hi again n again from rajeev', '2014-06-11 06:36:45', '1', '1'),
(36, 1, 'Admin', 'hello it is fine now', '2014-06-11 10:31:30', '2', '1'),
(37, 1, 'test@test.com', 'Yaa', '2014-06-16 00:52:59', '1', '1'),
(38, 1, 'test@test.com', 'Oh', '2014-06-16 00:53:08', '1', '1'),
(39, 1, 'Admin', 'ya', '2014-06-16 00:53:19', '2', '1'),
(40, 1, 'test@test.com', 'Hey', '2014-06-16 00:58:13', '1', '1'),
(41, 1, 'test@test.com', 'Yes', '2014-06-16 00:59:26', '1', '1'),
(42, 1, 'test@test.com', 'Nice', '2014-06-16 00:59:29', '1', '1'),
(43, 1, 'Admin', 'hoo', '2014-06-16 00:59:39', '2', '1'),
(44, 1, 'test@test.com', 'Ahan', '2014-06-16 01:00:12', '1', '1'),
(45, 1, 'Admin', 'This works!', '2014-06-16 01:00:23', '2', '1'),
(46, 10, 'Admin', 'hi from another salon', '2014-06-17 11:51:24', '2', '1'),
(47, 10, 'Admin', 'hiii', '2014-06-17 11:51:35', '2', '1'),
(48, 10, 'Admin', 'what happened?', '2014-06-17 11:52:28', '2', '1'),
(49, 10, 'Rajeev', 'testing new salon from another end', '2014-06-17 11:59:38', '1', '1'),
(50, 10, 'Rajeev', 'testing new salon from another end 2nd time', '2014-06-17 12:00:45', '1', '1'),
(51, 10, 'Admin', 'hiii', '2014-06-17 12:01:01', '2', '1'),
(52, 10, 'Admin', 'emantha ledhu le', '2014-06-17 12:01:08', '2', '1'),
(53, 1, 'test@test.com', 'Tesy', '2014-06-19 09:04:39', '1', '1'),
(54, 1, 'Admin', 'ya', '2014-06-19 09:04:53', '2', '1'),
(55, 1, 'test@test.com', 'Hey', '2014-07-03 05:42:52', '1', '1'),
(56, 1, 'test@test.com', 'Hdp', '2014-07-03 05:43:24', '1', '1'),
(57, 1, 'test@test.com', 'Hello', '2014-07-03 05:43:27', '1', '1'),
(58, 1, 'Admin', 'test', '2014-07-03 05:43:48', '2', '1'),
(59, 1, 'nithinredd@gmail.com', 'Hello', '2014-07-03 05:45:43', '1', '1'),
(60, 1, 'nithinredd@gmail.com', 'Hello from nithin', '2014-07-03 05:46:27', '1', '1'),
(61, 1, 'test@test.com', 'What', '2014-07-03 05:46:28', '1', '1'),
(62, 1, 'test@test.com', '123445', '2014-07-03 05:46:42', '1', '1'),
(63, 1, 'Admin', 'how does it work', '2014-07-03 05:46:46', '2', '1'),
(64, 1, 'nithinredd@gmail.com', 'Is it good?', '2014-07-03 05:47:01', '1', '1'),
(65, 1, 'test@test.com', 'Tyyu', '2014-07-07 05:34:32', '1', '1'),
(66, 1, 'Admin', 'test', '2014-07-07 05:34:57', '2', '1'),
(67, 1, 'Admin', 'hi', '2014-07-10 05:04:00', '2', '1'),
(68, 1, 'Rajeev', 'from mobile', '2014-07-10 05:09:21', '1', '1'),
(69, 1, 'awesome.nawesome@gmail.com', 'from mobile', '2014-07-10 07:23:42', '1', '1'),
(70, 1, 'admin@email.com', 'hiii', '2014-07-10 07:30:56', '2', '1'),
(71, 1, 'admin@email.com', 'bye', '2014-07-10 07:41:58', '2', '1'),
(72, 1, 'admin@email.com', 'hello', '2014-07-10 07:42:15', '2', '1'),
(73, 1, 'admin@email.com', 'hiii', '2014-07-10 08:04:10', '2', '1'),
(74, 1, 'admin@email.com', 'testing from admin side', '2014-07-10 08:04:47', '2', '1'),
(75, 1, 'admin@email.com', 'testing from admin side', '2014-07-10 08:05:41', '2', '1'),
(76, 1, 'admin@email.com', 'admin is testing it', '2014-07-10 08:17:56', '2', '1'),
(77, 1, 'admin@email.com', 'Hii', '2014-07-10 08:19:03', '2', '1'),
(78, 1, 'admin@email.com', 'this is xyz', '2014-07-10 08:19:52', '2', '1'),
(79, 1, 'admin@email.com', 'hello', '2014-07-10 08:24:04', '2', '1'),
(80, 1, 'admin@email.com', 'am here n u?', '2014-07-10 08:24:31', '2', '1'),
(81, 1, 'awesome.nawesome@gmail.com', 'from mobile', '2014-07-10 08:26:22', '1', '1'),
(82, 1, 'awesome.nawesome@gmail.com', 'from mobile', '2014-07-10 08:26:55', '1', '1'),
(83, 1, 'nithinredd@gmail.com', 'Ho', '2014-07-10 08:42:55', '1', '1'),
(84, 1, 'nithinredd@gmail.com', 'Yes', '2014-07-10 08:49:17', '1', '1'),
(85, 1, 'awesome.nawesome@gmail.com', 'from mobile', '2014-07-10 08:50:09', '1', '1'),
(86, 1, 'nithinredd@gmail.com', 'Tesy', '2014-07-10 08:50:34', '1', '1'),
(87, 1, 'nithinredd@gmail.com', 'Yvn', '2014-07-10 08:50:38', '1', '1'),
(88, 1, 'nithinredd@gmail.com', 'Cb', '2014-07-10 08:50:40', '1', '1'),
(89, 1, 'awesome.nawesome@gmail.com', 'from mobile', '2014-07-10 09:33:11', '1', '1'),
(90, 1, 'admin@email.com', 'hiii', '2014-07-10 09:46:13', '2', '1'),
(91, 1, 'admin@email.com', 'Test', '2014-07-10 09:46:17', '2', '1'),
(92, 1, 'admin@email.com', 'New', '2014-07-10 09:46:25', '2', '1'),
(93, 1, 'admin@email.com', 'Hello', '2014-07-10 09:46:53', '2', '1'),
(94, 1, 'admin@email.com', 'Testing', '2014-07-10 09:47:10', '2', '1'),
(95, 1, 'admin@email.com', 'New Test', '2014-07-10 09:48:56', '2', '1'),
(96, 1, 'admin@email.com', 'Testing', '2014-07-10 09:49:09', '2', '1'),
(97, 1, 'admin@email.com', 'Test 12345', '2014-07-10 09:49:43', '2', '1'),
(98, 1, 'admin@email.com', 'Test 245', '2014-07-10 09:50:01', '2', '1'),
(99, 1, 'admin@email.com', 'hii', '2014-07-10 09:52:34', '2', '1'),
(100, 1, 'admin@email.com', 'hello', '2014-07-10 09:53:22', '2', '1'),
(101, 1, 'awesome.nawesome@gmail.com', 'awesome', '2014-07-10 09:55:06', '1', '1'),
(102, 1, 'awesome.nawesome@gmail.com', 'hello', '2014-07-10 09:55:16', '1', '1'),
(103, 1, 'admin@email.com', 'Test', '2014-07-10 10:22:30', '2', '1'),
(104, 1, 'admin@email.com', 'yes', '2014-07-10 10:23:05', '2', '1'),
(105, 1, 'nithinredd@gmail.com', 'Hf', '2014-07-10 10:23:44', '1', '1'),
(106, 1, 'admin@email.com', 'afaf', '2014-07-10 10:23:49', '2', '1'),
(107, 1, 'admin@email.com', 'afa', '2014-07-10 10:24:20', '2', '1'),
(108, 1, 'admin@email.com', 'true', '2014-07-10 10:24:33', '2', '1'),
(109, 1, 'nithinredd@gmail.com', 'Yf', '2014-07-10 10:24:51', '1', '1'),
(110, 1, 'nithinredd@gmail.com', 'Fh', '2014-07-10 10:24:52', '1', '1'),
(111, 1, 'admin@email.com', 'af', '2014-07-10 10:27:28', '2', '1'),
(112, 1, 'admin@email.com', 'testing', '2014-07-10 10:27:34', '2', '1'),
(113, 1, 'nithinredd@gmail.com', 'Ggs', '2014-07-10 10:27:44', '1', '1'),
(114, 1, 'admin@email.com', 'aaf', '2014-07-10 10:27:48', '2', '1'),
(115, 1, 'nithinredd@gmail.com', 'Yddo', '2014-07-10 10:27:54', '1', '1'),
(116, 1, 'admin@email.com', 'affa', '2014-07-10 10:27:56', '2', '1'),
(117, 1, 'nithinredd@gmail.com', 'Hu', '2014-07-15 00:59:58', '1', '1'),
(118, 11, 'admin@email.com', 'Test', '2014-07-15 09:33:46', '2', '1'),
(119, 11, 'ram@test.com', 'Test', '2014-07-15 09:33:57', '1', '1'),
(120, 1, 'test@gmail.com', 'testing android', '2014-07-17 15:06:45', '1', '1'),
(121, 1, 'test@gmail.com', 'this for test', '2014-07-17 16:07:38', '1', '1'),
(122, 1, 'test@gmail.com', 'this is another test', '2014-07-17 16:09:46', '1', '1'),
(123, 1, 'test@gmail.com', 'hi frnds', '2014-07-17 16:17:11', '1', '1'),
(124, 1, 'test@gmail.com', 'hi how are u', '2014-07-17 16:22:45', '1', '1'),
(125, 1, 'test@gmail.com', 'hi how ', '2014-07-17 16:24:16', '1', '1'),
(126, 1, 'test@gmail.com', 'hello', '2014-07-17 20:09:26', '1', '1'),
(127, 1, 'nithinredd@gmail.com', 'hi', '2014-07-18 00:59:40', '1', '1'),
(128, 1, 'test@gmail.com', 'how are u', '2014-07-18 10:41:16', '1', '1'),
(129, 1, 'test@gmail.com', 'test', '2014-07-19 03:11:21', '1', '1'),
(130, 1, 'nithinredd@gmail.com', 'Tes', '2014-07-23 05:55:08', '1', '1'),
(131, 1, 'nithinredd@gmail.com', 'Ff', '2014-07-23 05:56:10', '1', '1'),
(132, 1, 'nithinredd@gmail.com', 'Ffh', '2014-07-23 05:56:45', '1', '1'),
(133, 1, 'nithinredd@gmail.com', 'Thh', '2014-07-23 05:58:24', '1', '1'),
(134, 1, 'nithinredd@gmail.com', 'Chnc', '2014-07-23 06:09:52', '1', '1'),
(135, 1, 'nithinredd@gmail.com', 'Shjf', '2014-07-23 06:09:57', '1', '1'),
(136, 1, 'admin@email.com', 'afaf', '2014-07-23 06:10:30', '2', '1'),
(137, 1, 'test@gmail.com', 'test', '2014-07-23 08:57:20', '1', '1'),
(138, 1, 'test@gmail.com', 'test2', '2014-07-23 09:41:45', '1', '1'),
(139, 1, 'nithinredd@gmail.com', ' hi', '2014-07-23 12:48:29', '1', '1'),
(140, 1, 'nithinredd@gmail.com', 'ys', '2014-07-23 12:48:34', '1', '1'),
(141, 1, 'nithinredd@gmail.com', 'Trd', '2014-07-23 12:49:13', '1', '1'),
(142, 1, 'test1@test.com', 'Yolo', '2014-07-23 14:16:13', '1', '1'),
(143, 1, 'test1@test.com', 'Ya', '2014-07-23 14:17:03', '1', '1'),
(144, 1, 'test@gmail.com', 'this for test', '2014-07-24 02:10:20', '1', '1'),
(145, 1, 'test@gmail.com', 'teat', '2014-07-24 02:22:37', '1', '1'),
(146, 1, 'test@gmail.com', 'test2', '2014-07-25 09:39:09', '1', '1'),
(147, 1, 'test@gmail.com', 'hiiiiii', '2014-07-25 13:30:31', '1', '1'),
(148, 1, 'admin@email.com', 'dilan', '2014-07-27 11:20:34', '2', '1'),
(149, 1, 'admin@email.com', 'hey', '2014-07-27 11:20:57', '2', '1'),
(150, 1, 'test1@test.com', 'What''s up', '2014-07-27 11:21:14', '1', '1'),
(151, 1, 'test1@test.com', 'Dffg', '2014-07-27 11:50:29', '1', '1'),
(152, 1, 'admin@email.com', 'stop', '2014-07-27 11:50:59', '2', '1'),
(153, 1, 'hamburgcoachneil@aol.com', 'whats up', '2014-07-27 11:51:05', '1', '1'),
(154, 1, 'test1@test.com', 'Yo', '2014-07-27 11:51:15', '1', '1'),
(155, 1, 'admin@email.com', 'test', '2014-07-27 11:51:35', '2', '1'),
(156, 1, 'hamburgcoachneil@aol.com', 'whats up dillan? can''t log in to website', '2014-07-27 23:26:16', '1', '1'),
(157, 1, 'test1@test.com', 'K', '2014-07-27 23:54:26', '1', '1'),
(158, 1, 'test1@test.com', 'I will send info', '2014-07-27 23:54:44', '1', '1'),
(159, 1, 'hamburgcoachneil@aol.com', 'mcKim r u there?', '2014-07-28 07:13:52', '1', '1'),
(160, 1, 'hamburgcoachneil@aol.com', 'whats ip', '2014-07-29 20:02:47', '1', '1'),
(161, 1, 'admin@email.com', 'ddd', '2014-07-31 08:31:58', '2', '1'),
(162, 1, 'admin@email.com', 'ddd', '2014-07-31 08:32:08', '2', '1'),
(163, 1, 'admin@email.com', 'ddd', '2014-07-31 08:32:38', '2', '1'),
(164, 1, 'admin@email.com', '3333', '2014-07-31 08:32:54', '2', '1'),
(165, 1, 'nithinredd@gmail.com', 'Hi', '2014-07-31 08:34:15', '1', '1'),
(166, 1, 'test1@test.com', 'Hey', '2014-07-31 08:50:35', '1', '1'),
(167, 1, 'hamburgcoachneil@aol.com', 'whats up', '2014-07-31 09:12:17', '1', '1'),
(168, 1, 'hamburgcoachneil@aol.com', 'the fbu guy wants to look at this so he can take it to his people', '2014-07-31 09:13:26', '1', '1'),
(169, 1, 'test1@test.com', 'Cool', '2014-07-31 09:15:51', '1', '1'),
(170, 1, 'test1@test.com', 'Test', '2014-07-31 09:24:14', '1', '1'),
(171, 1, 'admin@email.com', 'test', '2014-07-31 09:28:44', '2', '1'),
(172, 1, 'hamburgcoachneil@aol.com', 'when does this hit play store?', '2014-07-31 09:36:14', '1', '1'),
(173, 1, 'test1@test.com', 'Hold', '2014-07-31 09:36:31', '1', '1'),
(174, 1, 'hamburgcoachneil@aol.com', 'hold what?', '2014-07-31 09:46:20', '1', '1'),
(175, 1, 'nithinredd@gmail.com', 'Hi', '2014-08-04 05:51:30', '1', '1'),
(176, 1, 'admin@email.com', 'test', '2014-08-04 06:47:59', '2', '1'),
(177, 1, 'admin@email.com', 'hello', '2014-08-06 07:25:46', '2', '1'),
(178, 1, 'test1@test.com', 'Test', '2014-08-06 08:44:08', '1', '1'),
(179, 1, 'admin@email.com', 'i am nutz', '2014-08-06 11:36:39', '2', '1'),
(180, 1, 'Hamburgcoachneil@aol.com', 'Good to know ( that your nuts)..lol', '2014-08-06 11:54:17', '1', '1'),
(181, 1, 'hamburgcoachneil@aol.com', 'hey Neil, it''s Gary.  did u send me a msg.? ', '2014-08-06 12:34:02', '1', '1'),
(182, 1, 'hamburgcoachneil@aol.com', 'it just popped up on my cell!', '2014-08-06 12:34:34', '1', '1'),
(183, 1, 'hamburgcoachneil@aol.com', 'Gary u thereo', '2014-08-10 21:58:05', '1', '1'),
(184, 1, 'hamburgcoachneil@aol.com', 'Hey mckim pick up the phone', '2014-08-10 21:58:36', '1', '1'),
(185, 1, 'hamburgcoachneil@aol.com', 'Test', '2014-08-31 13:50:59', '1', '1'),
(186, 1, 'hamburgcoachneil@aol.com', 'Test 2', '2014-08-31 13:51:08', '1', '1'),
(187, 1, 'test@gmail.com', 'test', '2014-09-01 08:26:15', '2', '1'),
(188, 1, 'hamburgcoachneil@aol.com', 'Test 4', '2014-09-08 14:24:32', '1', '1'),
(189, 11, 'sport_admin', 'test', '2014-09-20 09:43:57', '2', '1'),
(190, 14, 'sport_admin', 'test', '2014-09-20 09:44:18', '2', '1'),
(191, 11, 'sport_admin', 'testing it again from this end', '2014-09-20 09:48:43', '2', '1'),
(192, 11, 'sport_admin', 'hi again...', '2014-09-20 09:50:10', '2', '1'),
(193, 11, 'sport_admin', 'hello', '2014-09-20 09:52:09', '2', '1'),
(194, 14, 'sport_admin', 'test 2', '2014-09-20 12:01:26', '2', '1'),
(195, 14, 'sport_admin', 'test', '2014-09-24 09:45:28', '2', '1'),
(196, 14, 'corneliusgeorge04@gmail.com', 'Test 3', '2014-09-25 14:07:24', '1', '1'),
(197, 14, 'corneliusgeorge04@gmail.com', 'Simer when you go to calendar on app it kicks u out', '2014-09-25 14:19:07', '1', '1'),
(198, 14, 'corneliusgeorge04@gmail.com', 'Any one from my midget team on this app yet?', '2014-10-01 13:53:33', '1', '1'),
(199, 14, 'portclay1378@msn.com', 'Gibbs...', '2014-10-02 11:35:05', '1', '1'),
(200, 14, 'corneliusgeorge04@gmail.com', 'Great', '2014-10-02 12:21:42', '1', '1'),
(201, 14, 'nicolefdonahue@yahoo.com', 'Donahue', '2014-10-02 14:13:15', '1', '1'),
(202, 14, 'tiffrlutz@hotmail.com', 'Lutz', '2014-10-02 17:02:08', '1', '1'),
(203, 14, 'pitugigi@hotmail.com', 'Sanchez', '2014-10-02 19:37:23', '1', '1'),
(204, 14, 'emendieta26@verizon.net', 'Garcia', '2014-10-02 22:40:42', '1', '1'),
(205, 14, 'corneliusgeorge04@gmail.com', 'All of you guys use IPhones?', '2014-10-04 07:13:06', '1', '1'),
(206, 14, 'emendieta26@verizon.net', 'I do', '2014-10-04 09:49:05', '1', '1'),
(207, 14, 'mstelluti@comcast.net', 'Yea', '2014-10-08 16:47:41', '1', '1'),
(208, 1, 'test@gmail.com', 'Dilan', '2014-10-09 19:33:48', '1', '1'),
(209, 1, 'sport_admin', 'did u get in', '2014-10-09 19:34:08', '2', '1'),
(210, 1, 'test@test.com', 'Is that good', '2014-10-09 20:35:18', '1', '1'),
(211, 1, 'sport_admin', 'test', '2014-10-10 07:16:34', '2', '1'),
(212, 15, 'sport_admin', 'test', '2014-10-10 12:21:30', '2', '1'),
(213, 14, 'corneliusgeorge04@gmail.com', 'Be patient still working out bugs', '2014-10-11 22:35:40', '1', '0'),
(214, 1, 'test@test.com', 'De', '2014-10-18 09:43:50', '1', '1'),
(215, 1, 'nithinredd@gmail.com', 'Hi', '2014-10-20 00:56:44', '1', '1'),
(216, 1, 'sport_admin', 'hey', '2014-10-20 00:58:27', '2', '1'),
(217, 1, 'sport_admin', 'hi', '2014-12-16 02:17:29', '2', '1'),
(218, 1, 'test@test.com', 'How are you', '2014-12-16 02:17:42', '1', '1'),
(219, 1, 'sport_admin', 'good..', '2014-12-16 02:17:50', '2', '1'),
(220, 1, 'sport_admin', 'test', '2014-12-16 02:18:06', '2', '1'),
(221, 1, 'sport_admin', 'abc', '2014-12-16 02:18:53', '2', '1'),
(222, 15, 'sport_admin', 'testing chat', '2014-12-16 04:44:15', '2', '1'),
(223, 1, 'sport_admin', 'test', '2014-12-16 04:54:18', '2', '1'),
(224, 1, 'sport_admin', 'test', '2014-12-16 04:56:50', '2', '1'),
(225, 1, 'sport_admin', 'asd', '2014-12-16 04:57:36', '2', '1'),
(226, 1, 'sport_admin', 'giii', '2014-12-16 04:58:12', '2', '1'),
(227, 1, 'sport_admin', 'hii', '2014-12-16 04:59:16', '2', '1'),
(228, 1, 'sport_admin', 'ho', '2014-12-16 05:00:43', '2', '1'),
(229, 1, 'sport_admin', 'haa', '2014-12-16 05:28:16', '2', '1'),
(230, 1, 'sport_admin', 'gaaa', '2014-12-16 05:28:56', '2', '1'),
(231, 1, 'sport_admin', 'jaaa', '2014-12-16 05:29:55', '2', '1'),
(232, 1, 'sport_admin', 'hoii', '2014-12-16 05:32:26', '2', '1'),
(233, 1, 'sport_admin', 'hello', '2014-12-16 05:35:23', '2', '1'),
(234, 1, 'sport_admin', 'check', '2014-12-16 05:40:32', '2', '1'),
(235, 1, 'sport_admin', 'check', '2014-12-16 05:40:40', '2', '1'),
(236, 1, 'sport_admin', 'nthng', '2014-12-16 05:49:33', '2', '1'),
(237, 1, 'sport_admin', 'trying....', '2014-12-16 06:05:33', '2', '1'),
(238, 1, 'sport_admin', 'test', '2014-12-16 08:13:58', '2', '1'),
(239, 1, 'sport_admin', 'ABC', '2014-12-16 08:16:07', '2', '1'),
(240, 1, 'sport_admin', 'Hi', '2014-12-16 08:18:56', '1', '1'),
(241, 1, 'sport_admin', 'Yes', '2014-12-16 08:19:06', '1', '1'),
(242, 14, 'corneliusgeorge04@gmail.com', 'Test', '2014-12-18 08:48:38', '1', '0'),
(243, 1, 'sport_admin', 'test1219', '2014-12-19 05:06:00', '1', '1'),
(244, 1, 'test@gmail.com', 'test1219425', '2014-12-19 05:55:55', '1', '1'),
(245, 1, 'sport_admin', 'check', '2014-12-22 03:56:47', '2', '1'),
(246, 1, 'sport_admin', 'test', '2014-12-22 03:59:24', '1', '1'),
(247, 1, 'sport_admin', 'check', '2014-12-22 04:00:10', '2', '1'),
(248, 1, 'sport_admin', 'hello...', '2014-12-22 04:43:09', '2', '1'),
(249, 1, 'sport_admin', 'test', '2014-12-22 06:55:26', '2', '1'),
(250, 1, 'sport_admin', 'check', '2014-12-22 06:56:42', '2', '1'),
(251, 1, 'nithinredd@gmail.com', 'Hi', '2014-12-22 06:57:09', '1', '1'),
(252, 1, 'sport_admin', 'hhjjhjhj', '2014-12-22 06:57:24', '2', '1'),
(253, 30, 'sport_admin', 'Yo', '2015-01-13 11:23:52', '1', '0'),
(254, 1, 'sport_admin', 'team', '2015-02-23 07:43:22', '2', '1'),
(255, 1, 'sport_admin', 'sports ', '2015-02-23 07:43:46', '2', '1'),
(256, 1, 'sport_admin', 'We will start soon!!', '2015-02-23 07:44:06', '2', '1'),
(257, 64, 'sport_admin', 'Hello', '2015-03-03 21:00:14', '1', '0'),
(258, 49, 'sport_admin', 'no practice', '2015-03-04 13:35:17', '2', '1'),
(259, 49, 'sport_admin', 'Why moy', '2015-03-04 13:35:43', '1', '1'),
(260, 49, 'sport_admin', 'wtf this app blows', '2015-03-04 14:28:28', '2', '1'),
(263, 11, 'sport_admin', 'test again', '2015-03-05 00:15:16', '2', '1'),
(262, 11, 'sport_admin', 'test', '2015-03-05 00:01:50', '2', '1'),
(264, 11, 'rajeev.uppala@gmail.com', 'Got it', '2015-03-05 00:15:38', '1', '1'),
(265, 11, 'rajeev.uppala@gmail.com', 'Good thing', '2015-03-05 00:16:11', '1', '1'),
(266, 11, 'srinivasulu.1216@gmail.com', 'hi', '2015-03-05 00:40:16', '1', '1'),
(267, 11, 'sport_admin', 'admin test', '2015-03-05 01:04:58', '2', '1'),
(268, 11, 'srinivasulu.1216@gmail.com', 'test', '2015-03-05 01:06:29', '1', '1'),
(269, 11, 'sport_admin', 'test', '2015-03-05 03:17:16', '2', '1'),
(270, 11, 'sport_admin', 'test', '2015-03-09 07:21:37', '2', '1'),
(271, 11, 'sport_admin', 'test', '2015-03-12 06:31:39', '2', '1'),
(272, 11, 'rajeev.uppala@gmail.com', 'Ok', '2015-03-12 06:32:04', '1', '1'),
(273, 11, 'sport_admin', 'this is fine', '2015-03-12 06:32:17', '2', '1'),
(274, 11, 'sport_admin', 'admin test', '2015-03-16 02:11:39', '2', '1'),
(275, 11, 'sport_admin', 'hi', '2015-03-25 07:40:35', '2', '1'),
(276, 11, 'rajeev.uppala@gmail.com', 'Hii', '2015-03-25 07:40:59', '1', '1'),
(277, 11, 'srinivasulu.1216@gmail.com', 'hiiii rrrr', '2015-03-25 07:41:21', '1', '1'),
(278, 11, 'srinivasulu.1216@gmail.com', 'hiiiiiii', '2015-03-25 07:41:46', '1', '1'),
(279, 11, 'srinivasulu.1216@gmail.com', 'cvbbb', '2015-03-25 07:42:06', '1', '1'),
(280, 11, 'srinivasulu.1216@gmail.com', 'bbbn', '2015-03-25 07:42:56', '1', '1'),
(281, 11, 'srinivasulu.1216@gmail.com', 'hhhhh', '2015-03-25 07:43:18', '1', '1'),
(282, 11, 'sport_admin', 'admin testing again', '2015-03-25 07:44:28', '2', '1'),
(283, 11, 'srinivasulu.1216@gmail.com', 'ttttessssss', '2015-03-25 08:14:39', '1', '1'),
(284, 11, 'rajeev.uppala@gmail.com', 'Ggggg', '2015-03-25 08:14:59', '1', '1'),
(285, 11, 'srinivasulu.1216@gmail.com', 'hi', '2015-03-25 08:15:43', '1', '1'),
(286, 11, 'srinivasulu.1216@gmail.com', 'hi', '2015-03-25 08:15:47', '1', '1'),
(287, 11, 'srinivasulu.1216@gmail.com', 'hjj', '2015-03-25 08:15:51', '1', '1'),
(288, 11, 'srinivasulu.1216@gmail.com', 'hhji', '2015-03-25 08:15:57', '1', '1'),
(289, 56, 'sport_admin', 'test', '2015-03-28 03:03:20', '2', '1'),
(290, 56, 'troberts@battlinminers.com ', 'hello chuck', '2015-03-31 20:19:55', '1', '1'),
(291, 103, 'sport_admin', 'tello', '2015-04-02 05:14:29', '2', '1'),
(292, 49, 'sport_admin', 'This app still blows', '2015-04-07 02:47:01', '1', '0'),
(293, 56, 'jmk827@hotmail.com', 'Hello', '2015-04-07 21:44:10', '1', '1'),
(294, 56, 'lilredhouse30@yahoo.com', 'Hi charlie', '2015-04-21 12:07:35', '1', '1'),
(295, 56, 'sport_admin', 'Yo Louise, just testing', '2015-04-22 20:59:41', '2', '1'),
(296, 56, 'lissaz3305@aol.com', 'thanks charlie!! I''m in.', '2015-04-22 21:58:49', '1', '1'),
(297, 107, 'sport_admin', 'Hi', '2015-05-11 01:44:49', '1', '1'),
(298, 11, 'sport_admin', 'hello', '2015-05-11 01:45:08', '2', '1'),
(299, 11, 'sport_admin', 'hello once again', '2015-05-11 01:45:39', '2', '1'),
(300, 107, 'sport_admin', 'Good morning', '2015-05-11 01:46:45', '1', '1'),
(301, 11, 'rajeev.uppala@gmail.com', 'good morning', '2015-05-11 02:01:51', '1', '1'),
(302, 11, 'rajeev.uppala@gmail.com', 'good morning', '2015-05-11 02:11:22', '1', '1'),
(303, 11, 'rajeev.uppala@gmail.com', 'good morning again', '2015-05-11 02:11:32', '1', '1'),
(304, 11, 'rajeev.uppala@gmail.com', 'Ok', '2015-05-11 02:20:25', '1', '1'),
(305, 11, 'sport_admin', 'ok?', '2015-05-11 02:20:46', '2', '1'),
(306, 11, 'rajeev.uppala@gmail.com', 'Yes', '2015-05-11 02:20:58', '1', '1'),
(307, 11, 'sport_admin', 'got it?', '2015-05-11 02:21:07', '2', '1'),
(308, 11, 'sport_admin', 'nonsense', '2015-05-11 02:21:20', '2', '1'),
(309, 11, 'sport_admin', 'h', '2015-05-11 02:24:40', '2', '1'),
(310, 11, 'rajeev.uppala@gmail.com', 'Hello', '2015-05-11 02:25:04', '1', '1'),
(311, 11, 'rajeev.uppala@gmail.com', 'I am chatting from app', '2015-05-11 02:25:21', '1', '1'),
(312, 73, 'sport_admin', 'Who''s there?', '2015-07-13 15:02:02', '1', '0'),
(313, 11, 'sport_admin', 'Admin', '2015-07-24 05:07:01', '1', '1'),
(314, 11, 'sport_admin', 'Ggfghh', '2015-07-24 05:07:22', '1', '1'),
(315, 11, 'rajeev.uppala@gmail.com', 'Ghdvjhgbb', '2015-07-24 05:08:00', '1', '1'),
(316, 11, 'sport_admin', 'nnnjnjnjknk', '2015-07-24 05:08:11', '2', '1'),
(317, 11, 'sport_admin', 'sosososoos', '2015-07-24 05:09:03', '2', '1'),
(318, 113, 'sport_admin', 'You there', '2015-07-28 04:44:24', '1', '0'),
(319, 1, 'hamburgcoachneil@aol.com', 'We need a small description of each function', '2015-08-02 09:31:04', '1', '0'),
(320, 1, 'hamburgcoachneil@aol.com', 'So people know how to use it', '2015-08-02 09:31:48', '1', '0'),
(321, 1, 'hamburgcoachneil@aol.com', 'I would like to see the back end?', '2015-08-02 09:33:40', '1', '0'),
(322, 1, 'hamburgcoachneil@aol.com', 'What is the live feed function? What does it do?', '2015-08-02 09:35:41', '1', '0'),
(323, 1, 'hamburgcoachneil@aol.com', 'What does it do', '2015-08-02 09:36:08', '1', '0'),
(324, 11, 'rajeev.uppala@gmail.com', 'Hi', '2015-08-04 07:01:24', '1', '1'),
(325, 11, 'srinu@emailemail.com', 'hm ok', '2015-08-04 07:01:49', '1', '1'),
(326, 11, 'rajeev.uppala@gmail.com', 'Hmm', '2015-08-04 07:01:56', '1', '1'),
(327, 11, 'srinu@emailemail.com', 'hhhjj', '2015-08-04 07:02:16', '1', '1'),
(328, 11, 'rajeev.uppala@gmail.com', 'Ccffgg', '2015-08-04 07:02:24', '1', '1'),
(329, 11, 'srinu@emailemail.com', 'hi', '2015-08-04 07:02:51', '1', '1'),
(330, 11, 'rajeev.uppala@gmail.com', 'Hhfdhj', '2015-08-04 07:03:05', '1', '1'),
(331, 11, 'sport_admin', 'abahahahah', '2015-08-04 07:03:27', '2', '1'),
(332, 14, 'corneliusgeorge04@gmail.com', 'Test', '2015-08-08 07:57:48', '1', '0'),
(333, 49, 'sport_admin', 'Hello', '2015-08-14 15:38:14', '1', '0'),
(334, 130, 'sport_admin', 'Chat', '2015-08-18 15:02:05', '1', '0'),
(335, 11, 'sport_admin', 'hello', '2015-09-02 01:34:02', '1', '0'),
(336, 143, 'sport_admin', 'test', '2015-10-27 20:14:08', '2', '1'),
(337, 148, 'sport_admin', 'ttt', '2015-10-28 06:57:17', '2', '1');

-- --------------------------------------------------------

--
-- Table structure for table `plus_modules`
--

CREATE TABLE IF NOT EXISTS `plus_modules` (
  `module_id` int(20) NOT NULL AUTO_INCREMENT,
  `module_type` enum('TeamMembers','DProducts','ContactUs','Specials','Menu','Gallery','Appointments','Products','Events','Sports_Desc','Files','Staff','Specials2','Club_Bottle_Service','Club_Registration','Groups','Training','Calendar','Chat') NOT NULL,
  `module_name` varchar(200) NOT NULL,
  `salon_id` int(20) NOT NULL,
  `show_to_employee` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1191 ;

--
-- Dumping data for table `plus_modules`
--

INSERT INTO `plus_modules` (`module_id`, `module_type`, `module_name`, `salon_id`, `show_to_employee`) VALUES
(583, 'TeamMembers', 'Team Members', 92, 1),
(584, 'Gallery', 'Gallery', 92, 1),
(585, 'Products', 'Gifts', 92, 1),
(586, 'Events', 'Events', 92, 1),
(587, 'Chat', 'Chat', 92, 1),
(588, 'Calendar', 'Calendar', 92, 1),
(589, 'Training', 'Training', 92, 1),
(590, 'Groups', 'Groups', 92, 1),
(591, 'Products', 'Store', 92, 1),
(592, 'TeamMembers', 'Team Members', 93, 1),
(593, 'Gallery', 'Gallery', 93, 1),
(594, 'Products', 'Gifts', 93, 1),
(595, 'Events', 'Events', 93, 1),
(596, 'Chat', 'Chat', 93, 1),
(597, 'Calendar', 'Calendar', 93, 1),
(598, 'Training', 'Training', 93, 1),
(599, 'Groups', 'Groups', 93, 1),
(600, 'Products', 'Store', 93, 1),
(601, 'TeamMembers', 'Team Members', 94, 1),
(602, 'Gallery', 'Gallery', 94, 1),
(603, 'Products', 'Gifts', 94, 1),
(604, 'Events', 'Events', 94, 1),
(605, 'Chat', 'Chat', 94, 1),
(606, 'Calendar', 'Calendar', 94, 1),
(607, 'Training', 'Training', 94, 1),
(608, 'Groups', 'Groups', 94, 1),
(609, 'Products', 'Store', 94, 1),
(755, 'TeamMembers', 'Team Members', 111, 1),
(756, 'Groups', 'Groups', 111, 1),
(757, 'Gallery', 'Gallery', 111, 1),
(758, 'Products', 'Gift Shop', 111, 1),
(759, 'Events', 'Events', 111, 1),
(760, 'Chat', 'Team Chat', 111, 1),
(761, 'Calendar', 'Calendar', 111, 1),
(762, 'Training', 'Training', 111, 1),
(763, 'Products', 'Store', 111, 1),
(764, 'TeamMembers', 'Team Members', 112, 1),
(765, 'Groups', 'Groups', 112, 1),
(766, 'Gallery', 'Gallery', 112, 1),
(767, 'Products', 'Gift Shop', 112, 1),
(768, 'Events', 'Events', 112, 1),
(769, 'Chat', 'Team Chat', 112, 1),
(770, 'Calendar', 'Calendar', 112, 1),
(771, 'Training', 'Training', 112, 1),
(772, 'Products', 'Store', 112, 1),
(773, 'TeamMembers', 'Team Members', 113, 1),
(774, 'Groups', 'Groups', 113, 1),
(775, 'Gallery', 'Gallery', 113, 1),
(776, 'Products', 'Gift Shop', 113, 1),
(777, 'Events', 'Events', 113, 1),
(778, 'Chat', 'Team Chat', 113, 1),
(779, 'Calendar', 'Calendar', 113, 1),
(780, 'Training', 'Training', 113, 1),
(781, 'Products', 'Store', 113, 1),
(791, 'TeamMembers', 'Team Members', 115, 1),
(792, 'Groups', 'Groups', 115, 1),
(793, 'Gallery', 'Gallery', 115, 1),
(794, 'Products', 'Gift Shop', 115, 1),
(795, 'Events', 'Events', 115, 1),
(796, 'Chat', 'Team Chat', 115, 1),
(797, 'Calendar', 'Calendar', 115, 1),
(798, 'Training', 'Training', 115, 1),
(799, 'Products', 'Store', 115, 1),
(800, 'TeamMembers', 'Team Members', 116, 1),
(801, 'Groups', 'Groups', 116, 1),
(802, 'Gallery', 'Gallery', 116, 1),
(803, 'Products', 'Gift Shop', 116, 1),
(804, 'Events', 'Events', 116, 1),
(805, 'Chat', 'Team Chat', 116, 1),
(806, 'Calendar', 'Calendar', 116, 1),
(807, 'Training', 'Training', 116, 1),
(808, 'Products', 'Store', 116, 1),
(883, 'TeamMembers', 'Team Members', 128, 1),
(884, 'Groups', 'Groups', 128, 1),
(885, 'Gallery', 'Gallery', 128, 1),
(886, 'Events', 'Events', 128, 1),
(887, 'Chat', 'Team Chat', 128, 1),
(888, 'Calendar', 'Calendar', 128, 1),
(889, 'Training', 'Training', 128, 1),
(897, 'TeamMembers', 'Team Members', 130, 1),
(898, 'Groups', 'Groups', 130, 1),
(899, 'Gallery', 'Gallery', 130, 1),
(900, 'Events', 'Events', 130, 1),
(901, 'Chat', 'Team Chat', 130, 1),
(902, 'Calendar', 'Calendar', 130, 1),
(903, 'Training', 'Training', 130, 1),
(911, 'TeamMembers', 'Team Members', 132, 1),
(912, 'Groups', 'Groups', 132, 1),
(913, 'Gallery', 'Gallery', 132, 1),
(914, 'Events', 'Events', 132, 1),
(915, 'Chat', 'Team Chat', 132, 1),
(916, 'Calendar', 'Calendar', 132, 1),
(917, 'Training', 'Training', 132, 1),
(918, 'TeamMembers', 'Team Members', 133, 1),
(919, 'Groups', 'Groups', 133, 1),
(920, 'Gallery', 'Gallery', 133, 1),
(921, 'Events', 'Events', 133, 1),
(922, 'Chat', 'Team Chat', 133, 1),
(923, 'Calendar', 'Calendar', 133, 1),
(924, 'Training', 'Training', 133, 1),
(925, 'TeamMembers', 'Team Members', 134, 1),
(926, 'Groups', 'Groups', 134, 1),
(927, 'Gallery', 'Gallery', 134, 1),
(928, 'Events', 'Events', 134, 1),
(929, 'Chat', 'Team Chat', 134, 1),
(930, 'Calendar', 'Calendar', 134, 1),
(931, 'Training', 'Training', 134, 1),
(960, 'TeamMembers', 'Team Members', 139, 1),
(961, 'Groups', 'Groups', 139, 1),
(962, 'Gallery', 'Gallery', 139, 1),
(963, 'Events', 'Events', 139, 1),
(964, 'Chat', 'Team Chat', 139, 1),
(965, 'Calendar', 'Calendar', 139, 1),
(966, 'Training', 'Training', 139, 1),
(974, 'TeamMembers', 'Team Members', 141, 1),
(975, 'Groups', 'Groups', 141, 1),
(976, 'Gallery', 'Gallery', 141, 1),
(977, 'Events', 'Events', 141, 1),
(978, 'Chat', 'Team Chat', 141, 1),
(979, 'Calendar', 'Calendar', 141, 1),
(980, 'Training', 'Training', 141, 1),
(988, 'TeamMembers', 'Team Members', 143, 1),
(989, 'Groups', 'Groups', 143, 1),
(990, 'Gallery', 'Gallery', 143, 1),
(991, 'Events', 'Events', 143, 1),
(992, 'Chat', 'Team Chat', 143, 1),
(993, 'Calendar', 'Calendar', 143, 1),
(994, 'Training', 'Training', 143, 1),
(995, 'TeamMembers', 'Team Members', 144, 1),
(996, 'Groups', 'Groups', 144, 1),
(997, 'Gallery', 'Gallery', 144, 1),
(998, 'Events', 'Events', 144, 1),
(999, 'Chat', 'Team Chat', 144, 1),
(1000, 'Calendar', 'Calendar', 144, 1),
(1001, 'Training', 'Training', 144, 1),
(1002, 'TeamMembers', 'Team Members', 145, 1),
(1003, 'Groups', 'Groups', 145, 1),
(1004, 'Gallery', 'Gallery', 145, 1),
(1005, 'Events', 'Events', 145, 1),
(1006, 'Chat', 'Team Chat', 145, 1),
(1007, 'Calendar', 'Calendar', 145, 1),
(1008, 'Training', 'Training', 145, 1),
(1009, 'TeamMembers', 'Team Members', 146, 1),
(1010, 'Groups', 'Groups', 146, 1),
(1011, 'Gallery', 'Gallery', 146, 1),
(1012, 'Events', 'Events', 146, 1),
(1013, 'Chat', 'Team Chat', 146, 1),
(1014, 'Calendar', 'Calendar', 146, 1),
(1015, 'Training', 'Training', 146, 1),
(1016, 'TeamMembers', 'Team Members', 147, 1),
(1017, 'Groups', 'Groups', 147, 1),
(1018, 'Gallery', 'Gallery', 147, 1),
(1019, 'Events', 'Events', 147, 1),
(1020, 'Chat', 'Team Chat', 147, 1),
(1021, 'Calendar', 'Calendar', 147, 1),
(1022, 'Training', 'Training', 147, 1),
(1023, 'TeamMembers', 'Team Members', 148, 1),
(1024, 'Groups', 'Groups', 148, 1),
(1025, 'Gallery', 'Gallery', 148, 1),
(1026, 'Events', 'Events', 148, 1),
(1027, 'Chat', 'Team Chat', 148, 1),
(1028, 'Calendar', 'Calendar', 148, 1),
(1029, 'Training', 'Training', 148, 1),
(1058, 'TeamMembers', 'Team Members', 153, 1),
(1059, 'Groups', 'Groups', 153, 1),
(1060, 'Gallery', 'Gallery', 153, 1),
(1061, 'Events', 'Events', 153, 1),
(1062, 'Chat', 'Team Chat', 153, 1),
(1063, 'Calendar', 'Calendar', 153, 1),
(1064, 'Training', 'Training', 153, 1),
(1065, 'TeamMembers', 'Team Members', 154, 1),
(1066, 'Groups', 'Groups', 154, 1),
(1067, 'Gallery', 'Gallery', 154, 1),
(1068, 'Events', 'Events', 154, 1),
(1069, 'Chat', 'Team Chat', 154, 1),
(1070, 'Calendar', 'Calendar', 154, 1),
(1071, 'Training', 'Training', 154, 1),
(1072, 'TeamMembers', 'Team Members', 155, 1),
(1073, 'Groups', 'Groups', 155, 1),
(1074, 'Gallery', 'Gallery', 155, 1),
(1075, 'Events', 'Events', 155, 1),
(1076, 'Chat', 'Team Chat', 155, 1),
(1077, 'Calendar', 'Calendar', 155, 1),
(1078, 'Training', 'Training', 155, 1),
(1079, 'TeamMembers', 'Team Members', 156, 1),
(1080, 'Groups', 'Groups', 156, 1),
(1081, 'Gallery', 'Gallery', 156, 1),
(1082, 'Events', 'Events', 156, 1),
(1083, 'Chat', 'Team Chat', 156, 1),
(1084, 'Calendar', 'Calendar', 156, 1),
(1085, 'Training', 'Training', 156, 1),
(1086, 'TeamMembers', 'Team Members', 157, 1),
(1087, 'Groups', 'Groups', 157, 1),
(1088, 'Gallery', 'Gallery', 157, 1),
(1089, 'Events', 'Events', 157, 1),
(1090, 'Chat', 'Team Chat', 157, 1),
(1091, 'Calendar', 'Calendar', 157, 1),
(1092, 'Training', 'Training', 157, 1),
(1093, 'TeamMembers', 'Team Members', 158, 1),
(1094, 'Groups', 'Groups', 158, 1),
(1095, 'Gallery', 'Gallery', 158, 1),
(1096, 'Events', 'Events', 158, 1),
(1097, 'Chat', 'Team Chat', 158, 1),
(1098, 'Calendar', 'Calendar', 158, 1),
(1099, 'Training', 'Training', 158, 1),
(1100, 'TeamMembers', 'Team Members', 159, 1),
(1101, 'Groups', 'Groups', 159, 1),
(1102, 'Gallery', 'Gallery', 159, 1),
(1103, 'Events', 'Events', 159, 1),
(1104, 'Chat', 'Team Chat', 159, 1),
(1105, 'Calendar', 'Calendar', 159, 1),
(1106, 'Training', 'Training', 159, 1),
(1107, 'TeamMembers', 'Team Members', 160, 1),
(1108, 'Groups', 'Groups', 160, 1),
(1109, 'Gallery', 'Gallery', 160, 1),
(1110, 'Events', 'Events', 160, 1),
(1111, 'Chat', 'Team Chat', 160, 1),
(1112, 'Calendar', 'Calendar', 160, 1),
(1113, 'Training', 'Training', 160, 1),
(1114, 'TeamMembers', 'Team Members', 161, 1),
(1115, 'Groups', 'Groups', 161, 1),
(1116, 'Gallery', 'Gallery', 161, 1),
(1117, 'Events', 'Events', 161, 1),
(1118, 'Chat', 'Team Chat', 161, 1),
(1119, 'Calendar', 'Calendar', 161, 1),
(1120, 'Training', 'Training', 161, 1),
(1121, 'TeamMembers', 'Team Members', 162, 1),
(1122, 'Groups', 'Groups', 162, 1),
(1123, 'Gallery', 'Gallery', 162, 1),
(1124, 'Events', 'Events', 162, 1),
(1125, 'Chat', 'Team Chat', 162, 1),
(1126, 'Calendar', 'Calendar', 162, 1),
(1127, 'Training', 'Training', 162, 1),
(1128, 'TeamMembers', 'Team Members', 163, 1),
(1129, 'Groups', 'Groups', 163, 1),
(1130, 'Gallery', 'Gallery', 163, 1),
(1131, 'Events', 'Events', 163, 1),
(1132, 'Chat', 'Team Chat', 163, 1),
(1133, 'Calendar', 'Calendar', 163, 1),
(1134, 'Training', 'Training', 163, 1),
(1135, 'TeamMembers', 'Team Members', 164, 1),
(1136, 'Groups', 'Groups', 164, 1),
(1137, 'Gallery', 'Gallery', 164, 1),
(1138, 'Events', 'Events', 164, 1),
(1139, 'Chat', 'Team Chat', 164, 1),
(1140, 'Calendar', 'Calendar', 164, 1),
(1141, 'Training', 'Training', 164, 1),
(1142, 'TeamMembers', 'Team Members', 165, 1),
(1143, 'Groups', 'Groups', 165, 1),
(1144, 'Gallery', 'Gallery', 165, 1),
(1145, 'Events', 'Events', 165, 1),
(1146, 'Chat', 'Team Chat', 165, 1),
(1147, 'Calendar', 'Calendar', 165, 1),
(1148, 'Training', 'Training', 165, 1),
(1149, 'TeamMembers', 'Team Members', 166, 1),
(1150, 'Groups', 'Groups', 166, 1),
(1151, 'Gallery', 'Gallery', 166, 1),
(1152, 'Events', 'Events', 166, 1),
(1153, 'Chat', 'Team Chat', 166, 1),
(1154, 'Calendar', 'Calendar', 166, 1),
(1155, 'Training', 'Training', 166, 1),
(1156, 'TeamMembers', 'Team Members', 167, 1),
(1157, 'Groups', 'Groups', 167, 1),
(1158, 'Gallery', 'Gallery', 167, 1),
(1159, 'Events', 'Events', 167, 1),
(1160, 'Chat', 'Team Chat', 167, 1),
(1161, 'Calendar', 'Calendar', 167, 1),
(1162, 'Training', 'Training', 167, 1),
(1163, 'TeamMembers', 'Team Members', 168, 1),
(1164, 'Groups', 'Groups', 168, 1),
(1165, 'Gallery', 'Gallery', 168, 1),
(1166, 'Events', 'Events', 168, 1),
(1167, 'Chat', 'Team Chat', 168, 1),
(1168, 'Calendar', 'Calendar', 168, 1),
(1169, 'Training', 'Training', 168, 1),
(1170, 'TeamMembers', 'Team Members', 169, 1),
(1171, 'Groups', 'Groups', 169, 1),
(1172, 'Gallery', 'Gallery', 169, 1),
(1173, 'Events', 'Events', 169, 1),
(1174, 'Chat', 'Team Chat', 169, 1),
(1175, 'Calendar', 'Calendar', 169, 1),
(1176, 'Training', 'Training', 169, 1),
(1177, 'TeamMembers', 'Team Members', 170, 1),
(1178, 'Groups', 'Groups', 170, 1),
(1179, 'Gallery', 'Gallery', 170, 1),
(1180, 'Events', 'Events', 170, 1),
(1181, 'Chat', 'Team Chat', 170, 1),
(1182, 'Calendar', 'Calendar', 170, 1),
(1183, 'Training', 'Training', 170, 1),
(1184, 'TeamMembers', 'Team Members', 171, 1),
(1185, 'Groups', 'Groups', 171, 1),
(1186, 'Gallery', 'Gallery', 171, 1),
(1187, 'Events', 'Events', 171, 1),
(1188, 'Chat', 'Team Chat', 171, 1),
(1189, 'Calendar', 'Calendar', 171, 1),
(1190, 'Training', 'Training', 171, 1);

-- --------------------------------------------------------

--
-- Table structure for table `plus_new_salon`
--

CREATE TABLE IF NOT EXISTS `plus_new_salon` (
  `new_salon_id` int(11) NOT NULL AUTO_INCREMENT,
  `new_salon_name` varchar(250) NOT NULL,
  `new_salon_email` varchar(250) NOT NULL,
  `new_salon_phone` varchar(250) NOT NULL,
  PRIMARY KEY (`new_salon_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_push_tokens`
--

CREATE TABLE IF NOT EXISTS `plus_push_tokens` (
  `token_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `push_token` text NOT NULL,
  `device_type` enum('1','2') NOT NULL DEFAULT '1',
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8965 ;

--
-- Dumping data for table `plus_push_tokens`
--

INSERT INTO `plus_push_tokens` (`token_id`, `salon_id`, `push_token`, `device_type`) VALUES
(8921, 130, 'f839a97b93e1585adb03a050fecf57e18512cbf9ecf426d2e51f771d3453afd8', '1'),
(8922, 130, '5757e427af62fd01aa3c650bafc9db6a4175c1c845931c6f7c415100417f253f', '1'),
(8928, 148, '0cd5b5f6f00c84af9f8b45f2ac9308dab060f1d5c7c8ccdb615842fb63c69d78', '1'),
(8929, 148, 'c185f8eff9973e2b7031e525f6122e8f085cfa7bd34d86e57f69f569e484e2fb', '1'),
(8930, 153, '0cd5b5f6f00c84af9f8b45f2ac9308dab060f1d5c7c8ccdb615842fb63c69d78', '1'),
(8931, 153, 'APA91bGg3fj0gAUKuaUtKPvwmsQcO5rRKd7tWrxcsDQ4Rvdu4-aNhrQBSz0lixxPfCQ6sh7JkNIdv3vWzidUG-J7eMRv5GydAGgs5HptDZwRyNGBUO_rSkw5xp4L634y8lcoLzrqyDGf', '2'),
(8932, 153, 'APA91bGNvmoBi_a3ilJYiTkehBqzoigpvwHkLt6QXErhWyGsSAm-5wTj3bBX8PNJvdRuhVNwRuo_MInhwJFagz2vlXt9VHIOusUoMXpx3usvgPFvJ0LCZHLAFR3lgddxLY3-0JCTajOG', '2'),
(8933, 148, 'APA91bGNvmoBi_a3ilJYiTkehBqzoigpvwHkLt6QXErhWyGsSAm-5wTj3bBX8PNJvdRuhVNwRuo_MInhwJFagz2vlXt9VHIOusUoMXpx3usvgPFvJ0LCZHLAFR3lgddxLY3-0JCTajOG', '2'),
(8934, 153, 'c185f8eff9973e2b7031e525f6122e8f085cfa7bd34d86e57f69f569e484e2fb', '1'),
(8935, 154, '0cd5b5f6f00c84af9f8b45f2ac9308dab060f1d5c7c8ccdb615842fb63c69d78', '1'),
(8936, 163, 'APA91bHQWzsxWCFj-wdGLYpxfZIX6-fko246N9xO_hv6hnraU0UjAy-NHqmxcbnic8pso1BzjrD0aNXB8281NiTcLwwg3-0k8-hiQDCsGvTqxlpYxyI9zh9KTUHn-qn6ayV5wHZUOogR', '2'),
(8937, 154, 'acb59a7edb9267ce5a9dffe1f498333be8626463b44a14c56ded053db059fad2', '1'),
(8938, 153, 'APA91bEcGAvdpHmwlBDwrlG95UtHzy8WyyZB2E7WkxosQGEymK7Wt0wwhu-1OlzioBwOUmFUxB4ZyBWyyYu4_Lp2Ua8yfzKYz1BTR2ZBWhomZZ9_YwxooINRZo2Wc7Q_4qmNvgbqOWzP', '2'),
(8939, 164, 'APA91bGrllnaeC2W9jBLOClv44zAyNFb5u3YKP9ei0Vg0zAnM29BLNnY0HUp4IFsWPyBfnaWCnASoOs9v6GzoCUcANDscLDR6F8XJBXrsgP4gWAWNoynFvRHXwlttFKr1dOmMy4PI0Nr', '2'),
(8940, 153, 'APA91bGEQ6gLTUVRwDQ2w0kj2o1rkooSwkxuz8AY3oV7jr68np5ZMvp-RUyU3BqI7hSk6V4wke9OuLCoG5aDT5TS8xgj7o0ULZ2daahirQ_SLk4MJ4_Sxmao8wVYST8ERtClA8AX0lBw', '2'),
(8941, 153, 'APA91bEWlj5HBAFkDgpB0JydfP-5xLYzPgOMpqAWzrVp3uVn2IsjLfyO_Xz9rYWB2dFwWDClsvCQT_TH1YCW6K_zACE_PXsqOBVeE1qQIrRmLbl9a0QWtQz5z9Xc8-P4KM7KKpRo-GLV', '2'),
(8942, 153, 'APA91bGwKrQVXjzB3Qo33QVMEOiWDVQ92TTwkKktMIms_D9uPjiDoXoMhEDUKo58yvS2qOXrGGnfjKWYXlP0r8wG5ir2YRf7eo9W-sv1cvs0IXmsnM3irGuQlj6yC820m6Ui-qhgy7ea', '2'),
(8943, 153, 'APA91bEnJbgYF8h1hEHuA-HhiRbnbaBaxtUGidV4NAnKNokl4xRe97oT1GxMtfBMmjmTStfh6cvDjjJ1kk7AzMYsjNLytn4VdQiT0lNgDuKNGT--E6aR-6z55tkUjnswIIFoWGG_dpRc', '2'),
(8944, 153, 'APA91bHljUiuVeWzparBgLY4naZuVGHbBjSEnaEOJ82FSJfSzY67H8tEzcH9RijYavzWabaM2lIQrzKSXlUYRpLauRlgqEzqoXExjxDHfm8X-FOGxfNru6VtrFN-hf2XtbhXd9h5Au3R', '2'),
(8945, 153, 'APA91bFk5ITsFoF30sTCjoaA13PUFV-3LQr98P8DweFHqpHI4dCxyCWmvPkQWzR2ng2dlmdS6eqKfIpjcQCxPn9pDhnszAk5B5Yyc45sM0kPuUBdBuM0QYSOK4kMyM8hWLO9eptnyhPI', '2'),
(8946, 153, 'APA91bF3WoxVkc-GcPglmA9OPwQ5k4pOOwrrNz9yJhjhkjIl_MPC7o0O5B1Ti5-0D6yStrckkgrjV74MTFUwa1K7gW-vho87eDbGNLs0qgOXokq0P8-yZcFTn1bjgvC-lTeanixjTVQL', '2'),
(8948, 153, 'APA91bFfgjScgs_8YdKcScMAYKrKlGNaTOJ2DjUucXIwrmxDuKgd6tc0HHeiRsB5bDuifbEflmOGEQXdy55v42P2OKP6_ZpkVwjmZdJhqcAhWc0APDQ5QkcUT_LcEB1GkpPM4mNQ9bot', '2'),
(8949, 153, 'APA91bFEVMrkoN41BBJU0TXOq6V6dfoTye8_n_VuQIi9L0Jj8FiyPPdJ9B9gZCTT0MNuBk_WfANuUeqO2kgTxRbBqo05nAjx4kW2vmaojWt_BCbi6UQCvuitSbvx7-9JDInXZH5VZFqY', '2'),
(8950, 153, 'APA91bHkXs9yYBfgLFeJptN-cTjbae_vaT2XsSzAVyGAWbGkirJXCghkZsVrgWyT9I7CvLHSL2xluCcCE1nqEi0LFRO_E7Dc53kJ6an3f8FQBO-VfkAJKnpiqeFv0C1VT7oijoyzSHQT', '2'),
(8951, 153, 'APA91bGHccYAXmXWHEEyLrbak9i5atI1GKuEVjfBefwFcX5oXhzjpwINByEnOUWKcCVGVpzC3oy0-e9D1_D3E2uHRXunM3I0rJmvhvcDiZtMw4Wl9b_KJAxiyHgLXNwnTWW9K2T6LCOB', '2'),
(8952, 153, 'APA91bH0Km9udXdaBsSKM6pIiuzKpg9vUHv9pD59EdCXR8PxOUXVqaWYeeuh9cqVm2kH62t4i5omyWYNw607gWByd8VrLGcNbOwpQgHW7mjdc5sO3mkLGPNUxtgkxjvSzE5vG51vYdE4', '2'),
(8953, 153, 'APA91bEm5NeDnAOi2n5ThE5HBCXTcFogqaa0uzb1rx6BFO9qOwB-gBzEs3WMOzcPNzdzzHJO73QYyC6RZGClZHdvw1FerMShcmY2GX5KUo5e89uqHbGOf2h6EK-invG_0qIVJOWb1HFj', '2'),
(8954, 153, 'APA91bEY-sdL21s4bRLjAYNISL2R8_bDTiZVXWONwW22J5eE16_mETp4GjBjVnPtzWGTYa2o7xyXfbwHCSrB5sMoIMS9ctgjLp84m0GgigO7r68Jnj3qxjMT3bkdnbJYstZBO34QIh7V', '2'),
(8955, 153, 'APA91bFTJMXQdhH_CfcfZBiOnuzpfILqaGhMaPjYBpR1UV6mrfNuYlK5gtb9riXY_e3w40fKEuhgmxzZM-ui5JhmLfM0dUA4ErBWSj02NF4p5S55dU0YJxBu2vchPZZl4LwNiIRDQtbd', '2'),
(8956, 153, 'APA91bHF5KDrPe_VzSwi29G4RixqXrT0UEt0AyNM3l-ddf80zX0f4B-ffjmn7sl6spc2TUH5sc7mocGIS4vB6IVXtX9BUPxtlOHAGkWMpS14nb_lUa6LSswQgVohTYfYz7RHRgaiE0kR', '2'),
(8957, 153, 'APA91bGaOi7gbe1bblyoxRiRkX-0vQ_WviG1sMtVOpE2wGe3IDqce23sMxbdtEg9LzIRj0muslU7Rwnc5zoruWQDhExqsmEcAVT_XmyCczB3_m7YbrDQ-uo8-BvW-0oiObtnxxJ9WkQP', '2'),
(8958, 153, 'APA91bFr5KrL-Fv7xtc-Xn-hKUHuAtQS1qUsOMFz9kDz7zMDCU1OhCLSreoKPlYWmmhcN9oeeO58T9eoeRPGicK7gBGx5ifq8371cQQyd4teMBZofIdbunO9b5kLIqcOcoozWrxubQWO', '2'),
(8959, 166, 'APA91bH_Dzz7Rd9hBecaD0rBCk__J6LNMcNG4WC87cLsZEbqQm55YREgbrDAMB4JdDc_xSopYlx-5T2XvUD9MTcHvxfasBNcoI7Mn4qVe8w4hoI9LoaItYfuhAYvR4Nt427CWajz8NqY', '2'),
(8960, 153, 'APA91bFKK1OBgpXnCL867fm74m3h2XVjoFF8-coBJ7YC54DGmMClQLalpSl28ow3ZiuM3NrZ6L_9mQ-Gs6u5BcC7cDgKkRRHQcNDgMIMbe-t0JsCZFadZ7UD-OeFMTEdN0jh8muTDB09', '2'),
(8961, 168, 'APA91bF7Pj608VZZl6bfSe766knDVyr3JT7BB7KNHC_aS2HVJhjUL9yRUTHjgAag75X3XMbds2eweWuMuS_GduI0E7CTxgqTQfDc_3FiaAgTzgLi5iNSWYCU5Ba_3xIsaRkd4siA1Y4r', '2'),
(8962, 168, 'acb59a7edb9267ce5a9dffe1f498333be8626463b44a14c56ded053db059fad2', '1'),
(8963, 153, 'APA91bEyuixBuDGLbyzQfICziwS51T5T6dVcBBGj1c7Mohca2wW6s6PTsEdgG3zWEgYE5VvgzVrDfnCHn8Iob0JdoxiFt_m1zy-9fUU2WRDndWeaMwJJmr15pScGSwyO8tqGxgqKc_BC', '2'),
(8964, 154, 'APA91bG652ZUalWPHW5SiQO5sjihx1z9tOf0WHAHL5b9d6TPINLto2i-gcbGN5lnin3FMLof6NhKFMv7lrP08oTpkSnK9P1ZCRKEuXwXa_4O2rA6-UfOrtQciX2Z9HDJ6oHrONTY86cY', '2');

-- --------------------------------------------------------

--
-- Table structure for table `plus_reviews`
--

CREATE TABLE IF NOT EXISTS `plus_reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `review_title` text NOT NULL,
  `review_description` text NOT NULL,
  `reviewer_name` varchar(99) NOT NULL,
  `reviewer_email` varchar(50) NOT NULL,
  `rating` decimal(10,0) NOT NULL,
  `review_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=204 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_salon`
--

CREATE TABLE IF NOT EXISTS `plus_salon` (
  `salon_id` int(11) NOT NULL AUTO_INCREMENT,
  `review_enabled` enum('No','Yes') NOT NULL,
  `email_push` enum('No','Yes') NOT NULL,
  `salon_name` varchar(100) NOT NULL,
  `salon_code` varchar(100) NOT NULL,
  `salon_contact_no` varchar(15) NOT NULL,
  `salon_email_id` varchar(100) NOT NULL,
  `salon_address` varchar(255) NOT NULL,
  `salon_zip` varchar(8) NOT NULL,
  `salon_city` varchar(100) NOT NULL,
  `salon_state` varchar(100) NOT NULL,
  `salon_url` varchar(100) NOT NULL,
  `salon_logo` varchar(255) NOT NULL,
  `salon_latitude` varchar(12) NOT NULL,
  `salon_longitude` varchar(12) NOT NULL,
  `android_app_id` varchar(50) NOT NULL,
  `apple_app_id` varchar(50) NOT NULL,
  `giftcards_enabled` enum('Yes','No') NOT NULL DEFAULT 'No',
  `giftcards_payment_gateway` enum('Auth.net','Paypal','Cpay') NOT NULL DEFAULT 'Auth.net',
  `referrals_enabled` enum('Yes','No') NOT NULL DEFAULT 'No',
  `reviews_enable` enum('Yes','No') NOT NULL DEFAULT 'No',
  `feedback_enabled` enum('Yes','No') NOT NULL DEFAULT 'No',
  `loyalty_enabled` enum('Yes','No') NOT NULL DEFAULT 'No',
  `millennium_enabled` enum('Yes','No') NOT NULL DEFAULT 'No',
  `texting_enabled` enum('Yes','No') NOT NULL DEFAULT 'No',
  `salon_status` enum('Active','Inactive') NOT NULL,
  `confirm_email` int(11) NOT NULL COMMENT '0=> not confirm , 1=>confirm',
  `salon_create_date` datetime NOT NULL,
  `salon_update_date` datetime NOT NULL,
  `salon_push_certificate` varchar(50) NOT NULL DEFAULT '',
  `salon_push_mode` enum('Production','Development') NOT NULL DEFAULT 'Development',
  `show_to_employee` varchar(15) NOT NULL DEFAULT '1,1,1,1,1,1,1,1',
  `website_template` varchar(200) NOT NULL DEFAULT 'rogers_like',
  `website_iframe` enum('Yes','No') NOT NULL DEFAULT 'No',
  `salon_friendly_name` varchar(150) NOT NULL,
  `mobile_iframe` enum('Yes','No') NOT NULL DEFAULT 'No',
  `is_directory` enum('1','0') NOT NULL,
  `integration` enum('Yes','No') NOT NULL DEFAULT 'No',
  `show_lottery` enum('Yes','No') NOT NULL DEFAULT 'No',
  `reports_url` text NOT NULL,
  `salon_iris` enum('Yes','No') NOT NULL,
  `salon_biz` enum('Yes','No') NOT NULL,
  `salon_biz_code` varchar(20) NOT NULL,
  `salon_biz_uname` varchar(100) NOT NULL,
  `salon_biz_pwd` varchar(100) NOT NULL,
  PRIMARY KEY (`salon_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=172 ;

--
-- Dumping data for table `plus_salon`
--

INSERT INTO `plus_salon` (`salon_id`, `review_enabled`, `email_push`, `salon_name`, `salon_code`, `salon_contact_no`, `salon_email_id`, `salon_address`, `salon_zip`, `salon_city`, `salon_state`, `salon_url`, `salon_logo`, `salon_latitude`, `salon_longitude`, `android_app_id`, `apple_app_id`, `giftcards_enabled`, `giftcards_payment_gateway`, `referrals_enabled`, `reviews_enable`, `feedback_enabled`, `loyalty_enabled`, `millennium_enabled`, `texting_enabled`, `salon_status`, `confirm_email`, `salon_create_date`, `salon_update_date`, `salon_push_certificate`, `salon_push_mode`, `show_to_employee`, `website_template`, `website_iframe`, `salon_friendly_name`, `mobile_iframe`, `is_directory`, `integration`, `show_lottery`, `reports_url`, `salon_iris`, `salon_biz`, `salon_biz_code`, `salon_biz_uname`, `salon_biz_pwd`) VALUES
(74, 'No', 'No', 'Saints', '1024695169', '', 'Hamburgvoachneil@aol.com', '', '', '', '', '', '', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-03-24 02:26:35', '2015-03-24 02:26:35', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(81, 'No', 'No', 'Demo23', '432336599', '', '', '', '', '', '', '', 'no_salon_image.gif', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-03-25 08:14:16', '2015-10-28 09:31:51', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(86, 'No', 'No', 'PA Swag SwagBacks 7v7', '385242449', '', 'sherri@paswag.net', '', '', '', '', '', '', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-03-26 13:55:34', '2015-03-26 13:55:34', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(92, 'No', 'No', 'Wilson Wrestling', '320555842', '6102071651', 'terencemckim@yahoo.com', 'West Lawn', '19608', '', 'PA', '', 'Bulldog-Athletics-Logo.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-03-30 14:02:29', '2015-03-30 14:02:29', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(93, 'No', 'No', 'Mnersville Little League', '1789182531', '570-617-6464', 'chasroberts@verizon.net', 'PO Box 684\nMinersville, PA', '17954', '', 'PA', '', '5.png', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-03-30 16:54:17', '2015-03-30 16:54:17', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(94, 'No', 'No', 'Holman Hurricanes 14U Elite', '1251939970', '484-769-0454', 'davejones@premierrents.com', '160 Scotland Dr', '19606', '', 'PA', '', '5.png', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-03-31 12:23:40', '2015-03-31 12:23:40', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(111, 'No', 'No', 'saints', '791576882', '', 'hamburgcoachneil@aol.com', '', '', '', '', '', '', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-04-08 12:14:08', '2015-04-08 12:14:08', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(112, 'No', 'No', 'Saints', '1167488702', '', 'Hamburgcoachneil@aol.com', '', '', '', '', '', '', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-04-14 10:42:08', '2015-04-14 10:42:08', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(113, 'No', 'No', 'Saints', '925783886', '6105872611', 'Hamburgcoachneil@aol.com', '309 pearl rd \nBernville ,Pa ', '19506', '', 'Pa.', '', '2.png', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-04-14 10:45:56', '2015-04-14 10:45:56', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(115, 'No', 'No', 'Berks Catholic', '1335341655', '610-780-5321', 'ruamazed@yahoo.com', '1234', '19518', '', 'pa', '', '2.png', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-06-19 17:22:26', '2015-06-19 17:22:26', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(128, 'No', 'No', 'Berks  Catholic Saints', '1568702309', '6105872611', 'Hamburgcoachneil@aol.com', '', '', '', '', 'Bcyfac@llmail.net', 'teamlogo_1439580529_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Inactive', 0, '2015-08-14 15:28:49', '2015-08-14 15:28:49', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(130, 'No', 'No', 'Wilson Soccer', '199653646', '6102071651', 'Terencemckim@yahoo.com', '1111', '12123', '123', '123', 'http://wjsc.org', 'teamlogo_1439584387_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-08-14 16:33:07', '2015-08-18 11:56:12', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(132, 'No', 'No', 'Terry', '1008402620', '6102071651', 'Terencemckim@yahoo.com', '222', '12121', '1', '1', 'www.wilsonsoccer.com', 'teamlogo_1442623984_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-09-18 20:53:04', '2015-10-27 20:06:34', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(133, 'No', 'No', 'warriors', '1335407174', '7034029165', 'gallantfx@live.com', 'e', '12123', 'Easton', 'PA', 'thegyfl.com', 'teamlogo_1443633702_1.jpg', 'e', 'e', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-09-30 13:21:42', '2015-10-27 20:02:03', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(134, 'No', 'No', 'Reading MS Black', '1753737007', '6106981114', 'howrobjr@aol.com', '33', '12121', '111', '11', 'www.readingsd.org', 'teamlogo_1444796331_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-14 00:18:51', '2015-10-27 20:04:27', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(139, 'No', 'No', 'Hannibal Falcons', '578430416', '5734069417', 'mohannibalfalcons@gmail.com', '12th Avenue', '80206', 'Denver', 'Co', 'www.hannibalfalcons.com', 'teamlogo_1445358672_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-20 12:31:12', '2015-10-27 20:00:15', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(141, 'No', 'No', 'Outsiderz', '1321732491', '4843186449', 'bc.outsiderz@gmail.com', 'test', '19103', '1', '191', 'www.berkscountyoutsiderz.com', 'teamlogo_1445373080_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-20 16:31:20', '2015-10-28 06:48:02', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(143, 'No', 'No', 'Terry', '1157563536', '6102071651', 'Terencemckim@yahoo.com', '111', '19107', 'Philadelphia', 'PA', 'www.wjsc.com', 'teamlogo_1445452793_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-21 14:39:53', '2015-10-27 20:07:01', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(144, 'No', 'No', 'Dane', '1286716132', '4842696687', 'Danemichael.miller@gmail.com', '1218 Locust St', '19107', 'Philadelphia', 'PA', 'Www.Garagestrength.com', 'teamlogo_1445567355_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-22 22:29:16', '2015-10-27 19:57:16', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(145, 'No', 'No', 'Dane', '2046189272', '4842696687', 'danemichael.miller@gmail.com', '33', '12121', '1', '11', 'www.GarageStrength.com', 'teamlogo_1445602135_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-23 08:08:56', '2015-10-27 20:11:08', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(146, 'No', 'No', 'Neil George', '2141339832', '6105872611', 'Brokklee@aol.com', '1218 Locust St', '19107', 'Philadelphia', 'PA', 'Www.smallplayerbigplay.com', 'teamlogo_1445602844_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-23 08:20:44', '2015-10-27 19:31:34', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(147, 'No', 'No', 'Garage Strength', '2081710484', '4842696687', 'Dane@garagestrength.com', '1218 Locust St', '19107', 'Philadelphia', 'PA', 'Www.GarageStrength.com', 'teamlogo_1445602867_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-23 08:21:07', '2015-10-27 19:31:05', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(148, 'No', 'No', 'Dilan', '1584576037', '2672074190', 'Dilan@Webappclouds.com', '219 winterwood lane', '08062', 'mullica hill', 'New Jersey', 'Salon.com', 'teamlogo_1445988047_1.jpg', '1', '1', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-27 19:20:47', '2015-10-27 19:32:15', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(153, 'No', 'No', 'test gamer', '1702561178', '1234567890', 'kranthi@webappclouds.com', 'Webappclouds, Hyderabad, India', '500038', 'Hyderabad', 'Telangana', 'sportyfox.com', '6.png', '17.4315278', '78.4467743', 'com.webappclouds.sportyfox', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2015-10-28 11:58:34', '2015-11-17 07:36:49', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(154, 'No', 'No', 'Dilan', '355183164', '2155551212', 'Dilan@Webappclouds.com', '', '', '', '', 'Www', 'teamlogo_1446111550_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-29 05:39:10', '2015-10-29 05:39:10', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(155, 'No', 'No', 'dhdh', '44099685', '1236456987', 'kranthi@webappclouds.com', '', '', '', '', 'hfhdhhxjd.com', 'teamlogo_1446113996_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-29 06:19:56', '2015-10-29 06:19:56', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(158, 'No', 'No', 'panthers', '1713507758', '1234567890', 'kranthi.goli@yahoo.co.in', '', '', '', '', 'www.dog.com', 'teamlogo_1446121006_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-10-29 08:16:46', '2015-10-29 08:16:46', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(159, 'No', 'No', 'CATS', '439175842', '2679713447', 'cobbscreekcats@yahoo.com', '', '', '', '', 'www.cobbscreekcats.tigersix.net', 'teamlogo_1447633015_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-15 19:16:55', '2015-11-15 19:16:55', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(160, 'No', 'No', 'CATS', '470534713', '2679713447', 'cobbscreekcats@yahoo.com', '', '', '', '', 'www.eteamz.com/cobbscreekcats', 'teamlogo_1447633261_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-15 19:21:01', '2015-11-15 19:21:01', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(161, 'No', 'No', 'ATester', '1738746906', '1234567890', 'aman@webappclods.com', '', '', '', '', 'abc', 'teamlogo_1447652832_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-16 00:47:12', '2015-11-16 00:47:12', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(162, 'No', 'No', 'web', '1365025301', '1234567890', 'web@gmail.com', '', '', '', '', 'tester.com', 'teamlogo_1447653270_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-16 00:54:30', '2015-11-16 00:54:30', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(163, 'No', 'No', 'Neil George', '1387933999', '6105872611', 'Hamburgcoachneil@aol.com', '', '', '', '', 'Www.smallplayerbigplay.com', 'teamlogo_1447684466_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-16 09:34:27', '2015-11-16 09:34:27', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(164, 'No', 'No', 'Neil George', '1723968587', '6105872611', 'Corneliusgeorge04@gmail.com', '', '', '', '', 'Smallplayerbigplay.com', 'teamlogo_1447748325_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-17 03:18:45', '2015-11-17 03:18:45', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(165, 'No', 'No', 'Wilson  HS', '2129848066', '6105072549', 'Ken.thomason67@gmail.com', '', '', '', '', 'http://wilsonbulldogwrestling.net', 'teamlogo_1447789547_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-17 14:45:48', '2015-11-17 14:45:48', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(166, 'No', 'No', 'amantest', '573410889', '1234567890', 'aman@webappclouds.com', '', '', '', '', 'webappclouds.com', 'teamlogo_1448428934_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-11-25 00:22:14', '2015-11-25 00:22:14', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(167, 'No', 'No', 'HG Team', '1830346265', '000', 'hgteam@gmail.com', '', '', '', '', 'www.hg.game.net', 'teamlogo_1450100188_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-12-14 08:36:28', '2015-12-14 08:36:28', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(168, 'No', 'No', 'sos Elite (Seven on Seven)', '1498898338', '4847690454', 'davejones@premierrents.com', '', '', '', '', 'na', 'teamlogo_1450848030_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-12-23 00:20:30', '2015-12-23 00:20:30', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(169, 'No', 'No', 'Test', '1375794572', '1234567890', 'amankaler0@gmail.com', '', '', '', '', 'www.webappclouds.com', 'teamlogo_1451545750_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2015-12-31 02:09:10', '2015-12-31 02:09:10', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(170, 'No', 'No', 'Vols', '110193957', '8655882465', 'smokyweb@gmail.com', '', '', '', '', 'knoxweb.com', 'teamlogo_1452611016_1.jpg', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 0, '2016-01-12 10:03:36', '2016-01-12 10:03:36', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', ''),
(171, 'No', 'No', 'saints', '1321553479', '', 'hamburgcoachneil@aol.com', '', '', '', '', '', '', '', '', '', '', 'No', 'Auth.net', 'No', 'No', 'No', 'No', 'No', 'No', 'Active', 1, '2016-02-01 22:47:35', '2016-02-01 22:47:35', '', 'Development', '1,1,1,1,1,1,1,1', 'rogers_like', 'No', '', 'No', '1', 'No', 'No', '', 'Yes', 'Yes', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `plus_salon_directory`
--

CREATE TABLE IF NOT EXISTS `plus_salon_directory` (
  `directory_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `directory_fb` varchar(255) NOT NULL,
  `directory_twitter` varchar(255) NOT NULL,
  `directory_google` varchar(255) NOT NULL,
  PRIMARY KEY (`directory_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `plus_salon_directory`
--

INSERT INTO `plus_salon_directory` (`directory_id`, `salon_id`, `directory_fb`, `directory_twitter`, `directory_google`) VALUES
(50, 1, '0', '0', '0'),
(51, 11, '0', '0', '0'),
(52, 10, '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `plus_settings`
--

CREATE TABLE IF NOT EXISTS `plus_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appfigures_email` varchar(200) NOT NULL,
  `appfigures_password` varchar(200) NOT NULL,
  `enable_sendgrid` int(1) NOT NULL DEFAULT '0',
  `sendgrid_api_user` varchar(200) NOT NULL,
  `sendgrid_api_key` varchar(200) NOT NULL,
  `sendgrid_api_key_id` text NOT NULL,
  `android_push_api_key` varchar(200) NOT NULL,
  `terms_and_conditions_url` text NOT NULL,
  `super_admin` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `plus_settings`
--

INSERT INTO `plus_settings` (`id`, `appfigures_email`, `appfigures_password`, `enable_sendgrid`, `sendgrid_api_user`, `sendgrid_api_key`, `sendgrid_api_key_id`, `android_push_api_key`, `terms_and_conditions_url`, `super_admin`) VALUES
(1, '', '', 1, '', '', '', '', '', 'hamburgcoachneil@aol.com');

-- --------------------------------------------------------

--
-- Table structure for table `plus_sports_desc`
--

CREATE TABLE IF NOT EXISTS `plus_sports_desc` (
  `sports_desc_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `sports_desc_desc` text NOT NULL,
  `sports_desc_add_date` datetime NOT NULL,
  `sports_desc_update_date` datetime NOT NULL,
  PRIMARY KEY (`sports_desc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_team_members`
--

CREATE TABLE IF NOT EXISTS `plus_team_members` (
  `teammembers_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `teammembers_name` varchar(100) NOT NULL,
  `teammembers_email` varchar(100) NOT NULL,
  `teammembers_password` varchar(100) NOT NULL,
  `cometchat_userId` int(11) NOT NULL,
  `push_token` varchar(255) NOT NULL,
  `teammembers_image` varchar(200) NOT NULL,
  `teammembers_desc` text NOT NULL,
  `login_times` int(11) NOT NULL,
  `teammembers_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `teammembers_create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `teammembers_update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `teammembers_order` int(11) NOT NULL DEFAULT '0',
  `teammembers_phone` varchar(250) NOT NULL,
  `teammembers_parentname` varchar(250) NOT NULL COMMENT 'Father Name',
  `teammembers_parentemail` varchar(250) NOT NULL COMMENT 'Father Email',
  `teammembers_parentpass` varchar(250) NOT NULL COMMENT 'Father password',
  `teammembers_parentphn` varchar(250) NOT NULL COMMENT 'Father phone',
  `mother_name` varchar(250) NOT NULL,
  `mother_email` varchar(250) NOT NULL,
  `mother_pass` varchar(250) NOT NULL,
  `mother_phone` varchar(250) NOT NULL,
  `teammembers_sendmail` int(11) NOT NULL,
  PRIMARY KEY (`teammembers_id`),
  KEY `teammembers_id` (`teammembers_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=276 ;

--
-- Dumping data for table `plus_team_members`
--

INSERT INTO `plus_team_members` (`teammembers_id`, `module_id`, `salon_id`, `teammembers_name`, `teammembers_email`, `teammembers_password`, `cometchat_userId`, `push_token`, `teammembers_image`, `teammembers_desc`, `login_times`, `teammembers_status`, `teammembers_create_date`, `teammembers_update_date`, `teammembers_order`, `teammembers_phone`, `teammembers_parentname`, `teammembers_parentemail`, `teammembers_parentpass`, `teammembers_parentphn`, `mother_name`, `mother_email`, `mother_pass`, `mother_phone`, `teammembers_sendmail`) VALUES
(7, 1, 1, 'vinodkumar', 'vinod.oditi@gmail.com', 'test1234', 0, '147', 'teammembers_13987013941444217896.png', 'test', 0, 'Active', '2014-04-28 12:09:54', '2014-08-12 10:08:05', 1, '234345345', '', '', '', '', '', '', '', '', 0),
(6, 1, 1, 'pankaj', 'pankaj.oditi@gmail.com', 'test1234', 0, 'APA91bGGuPu_K1lXaSqJpNNyGu8i_zLBmh6T5HtXATFNNGqivFINYnx6vkBqM-hqiEfefS2dhH5Bm-To752xXVRbIxQqeNtWB3AcVOjAu6AI6Qi1ePLkLP0n-zlBsYhjQhI2nchyHe3LXRBg0xw_wLOiUhzNICKO3YudqpsmOlfC8PDk3_1d9ac', 'teammembers_14073391201906564284.png', 'test', 0, 'Active', '2014-04-28 07:15:01', '2014-08-12 10:07:17', 1, '65436357', '', '', '', '', '', '', '', '', 0),
(8, 1, 1, 'test', 'test@gmail.com', '1234567', 18, 'APA91bHZE7OrU2UR6GFeTWQXpkF2CLUOgG_wnvH7n1W41uZow924W-QuZdyDhtiq88u28qdnwKYQIKkkFCbhJ5geuKFoaWqxejxbpFlTFO58J87DB9GZkJ3_jAaNZJ4VpZhwCWsjtWYB', 'User1.png', 'test', 3, 'Active', '2014-07-03 05:45:11', '2014-10-09 19:32:14', 1, '777756765', '', '', '', '', '', '', '', '', 1),
(87, 432, 14, 'Andrew Chiarelli', 'amychiarelli@msn.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:05:54', '2014-09-11 10:05:54', 1, '6107809803', '', '', '', '', '', '', '', '', 1),
(9, 1, 1, 'newtest1234', 'vinod.od@gmail.com', 'test1234', 0, 'APA91bGlSPh2OJkipe6bNd8UGcbScRH4ZeYeEJZ_hIClo9KbiK-GRXvgRMgithBAbFN8Yyb80kZNpsiEgt7m-yMvK1ylHuz1wElTrTEsZKU2BywJ_KwU23N38t-0ZKXZDyqq-C-WuVaYlIGgpix2596sBkFWyHFZMVvf5ALv-cgMHWixz1ebv2A\n', 'User1.png', 'test', 0, 'Active', '2014-07-03 05:45:11', '2014-08-12 10:00:20', 1, '35345345', '', '', '', '', '', '', '', '', 0),
(86, 1, 1, 'Nithin', 'nithinredd@gmail.com', 'password', 15, '', 'User1.png', 'Desc', 4, 'Active', '2014-08-27 08:49:38', '2014-08-27 08:49:38', 1, '', '', '', '', '', '', '', '', '', 1),
(146, 1, 1, 'Srinu Android Test', 'srinivasulu.1216@gmail.com', '1234', 17, 'c185f8eff9973e2b7031e525f6122e8f085cfa7bd34d86e57f69f569e484e2fb', 'User1.png', 'Android Developer Test', 0, 'Active', '2015-07-09 02:56:49', '2015-07-09 02:56:49', 1, '', '', '', '', '', '', '', '', '', 1),
(71, 1, 1, 'jen', 'jen@gmail.com', 'test1234', 0, '', 'teammembers_140738896015288826.png', 'test', 0, 'Active', '2014-08-07 01:22:40', '2014-08-12 10:11:41', 1, '35345345', '', '', '', '', '', '', '', '', 0),
(88, 432, 14, 'Joey Conlon', 'steph0117@hotmail.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:07:57', '2014-09-11 10:07:57', 1, '6103853091', '', '', '', '', '', '', '', '', 1),
(89, 432, 14, 'Ryan Coughenour', 'roxanne_arb@yahoo.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:09:02', '2014-09-25 14:49:36', 1, '6103851443', '', '', '', '', '', '', '', '', 1),
(85, 1, 1, 'coach neil', 'hamburgcoachneil@aol.com', 'hello@213', 0, 'acb59a7edb9267ce5a9dffe1f498333be8626463b44a14c56ded053db059fad2', 'User1.png', 'hamburgcoachneil@aol.com', 4, 'Active', '2014-08-27 06:58:45', '2015-08-01 23:36:58', 1, '', '', '', '', '', '', '', '', '', 0),
(90, 432, 14, 'Earl Deschamps', 'fivkoolkidz@aol.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:10:27', '2014-09-11 10:10:27', 1, '6103745049', '', '', '', '', '', '', '', '', 1),
(82, 1, 1, 'test', 'vinod.od@gmail.com', 'test1234', 0, '', 'teammembers_20478427271407852020.png', 'test', 0, 'Active', '2014-08-12 09:56:26', '2014-08-12 10:03:43', 1, '35345345', '', '', '', '', '', '', '', '', 0),
(84, 1, 1, 'ranjit test', 'test@test.com', '1234567', 0, '33924ac1bef84a4811a19a8545aab33bc3944465ec16a09bd842757291b5164d', 'User1.png', 'test', 0, 'Active', '2014-08-13 08:56:32', '2014-10-09 20:03:20', 1, '3435345', '', '', '', '', '', '', '', '', 1),
(91, 432, 14, 'Raymond Deschamps', 'fivkoolkidz@aol.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:17:16', '2014-09-11 10:17:16', 1, '6103745049', '', '', '', '', '', '', '', '', 1),
(92, 432, 14, 'Roman Donahue', 'nicolefdonahue@yahoo.com', 'test1234', 0, '5dd6c37f2ccafe5cb8a0ce78f9cb727012f2dbb18f5cf6e6a951ffd26cfd7dff', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:18:59', '2014-09-11 10:18:59', 1, '6105688814', '', '', '', '', '', '', '', '', 1),
(93, 432, 14, 'Michael Fioravante', 'slposch@yahoo.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:20:16', '2014-09-11 10:20:16', 1, '6107961045', '', '', '', '', '', '', '', '', 0),
(94, 432, 14, 'Brigham Kobularcik', 'brian@mmgapts.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:20:53', '2014-09-11 10:20:53', 1, '6107774552', '', '', '', '', '', '', '', '', 1),
(95, 432, 14, 'Dominic Formando*injured/quit', 'kathyf2294@hotmail.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:21:35', '2014-09-11 10:21:35', 1, '6107792858', '', '', '', '', '', '', '', '', 0),
(96, 432, 14, 'Jack Krishock', 'mkrishock@comcast.net', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:22:03', '2014-09-11 10:22:03', 1, '6109294555', '', '', '', '', '', '', '', '', 1),
(97, 432, 14, 'Jase Fowler', 'jam_fow@msn.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:22:35', '2014-09-11 10:23:58', 1, '6107960455', '', '', '', '', '', '', '', '', 0),
(98, 432, 14, 'Gabe Leskusky', 'cmalf10@aol.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:23:18', '2014-09-11 10:23:18', 1, '6109294555', '', '', '', '', '', '', '', '', 1),
(99, 432, 14, 'Lincoln Lutz', 'tiffrlutz@hotmail.com', 'test1234', 0, '6a6ce589c1f27711f87b4cf4bfff64bb1699371f394771719633d9e77ab40484', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:24:11', '2014-09-11 10:24:11', 1, '6104516444', '', '', '', '', '', '', '', '', 1),
(100, 432, 14, 'Andrew McConnell', 'kamkpm@aol.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:24:57', '2014-09-11 10:24:57', 1, '6106789280', '', '', '', '', '', '', '', '', 1),
(101, 432, 14, 'Javier Garcia', 'emendieta26@verizon.net', 'test1234', 0, '034413b9c09104dab2e89b9eb5cd3f4db1a7831805dee69f1c93f9a8518a2ed7', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:24:58', '2014-09-11 10:24:58', 1, '6107363853', '', '', '', '', '', '', '', '', 0),
(102, 432, 14, 'Noe Mingucha', 'maruca1174@gmail.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:25:49', '2014-09-11 10:25:49', 1, '610.823.4915', '', '', '', '', '', '', '', '', 1),
(103, 432, 14, 'Clayton Gibbs', 'portclay1378@msn.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:26:01', '2014-09-11 10:26:01', 1, '6105079538', '', '', '', '', '', '', '', '', 0),
(104, 432, 14, 'Simeon Miravich', 'pmiravich@comcast.net', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:26:34', '2014-09-11 10:26:34', 1, '6107794543', '', '', '', '', '', '', '', '', 1),
(105, 432, 14, 'Jackson Gofus', 'bryangofus@comcast.net', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:27:02', '2014-09-11 10:27:02', 1, '6107796616', '', '', '', '', '', '', '', '', 0),
(106, 432, 14, 'Anthony Myers', 'jdiblasimyers@yahoo.com', 'test1234', 0, '0e36e616f85146147913c8b6aaff32af3bb1315d386a7afa790e3ad514a9a715', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:27:16', '2014-09-11 10:27:16', 1, '6103765148', '', '', '', '', '', '', '', '', 1),
(107, 432, 14, 'Conner Gundersen', 'pgundersen@verizon.net', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:27:57', '2014-09-11 10:27:57', 1, '6109279844', '', '', '', '', '', '', '', '', 0),
(108, 432, 14, 'Zachary Renninger', 'jennmetalhead@yahoo.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:28:00', '2014-09-11 10:28:00', 1, '6103740148', '', '', '', '', '', '', '', '', 1),
(109, 432, 14, 'Fabio Reyes', 'prontogiros@hotmail.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:29:08', '2014-09-11 10:29:08', 1, '6103723701', '', '', '', '', '', '', '', '', 1),
(110, 432, 14, 'Owen Wolfe', 'elizabeth.wolfe3@gmail.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:29:39', '2014-09-11 10:29:39', 1, '6109267085', '', '', '', '', '', '', '', '', 0),
(111, 432, 14, 'St. Peter''s Kevin', 'pitugigi@hotmail.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:29:55', '2014-09-11 10:29:55', 1, '6107637395', '', '', '', '', '', '', '', '', 1),
(112, 432, 14, 'Darren Sell', 'lsdsell@msn.com', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:30:42', '2014-09-11 10:30:42', 1, '6107753105', '', '', '', '', '', '', '', '', 1),
(113, 432, 14, 'James Stelluti', 'mstelluti@comcast.net', 'test1234', 0, '', 'User1.png', 'test', 0, 'Active', '2014-09-11 10:30:45', '2014-09-11 10:30:45', 1, '6103012742', '', '', '', '', '', '', '', '', 0),
(114, 432, 14, 'c j carwill', 'chamacarwll@yahoo.com', 'cj1425', 0, '', 'User1.png', 'midget', 0, 'Active', '2014-09-13 17:26:07', '2014-09-20 09:13:13', 1, '', '', '', '', '', '', '', '', '', 1),
(115, 432, 14, 'stacie george', 'bugginhome@aol.com', 'brandon06', 0, 'APA91bGrllnaeC2W9jBLOClv44zAyNFb5u3YKP9ei0Vg0zAnM29BLNnY0HUp4IFsWPyBfnaWCnASoOs9v6GzoCUcANDscLDR6F8XJBXrsgP4gWAWNoynFvRHXwlttFKr1dOmMy4PI0Nr', 'User1.png', 'mom', 0, 'Active', '2014-09-13 17:32:25', '2014-09-13 17:32:25', 1, '6105872612', '', '', '', '', '', '', '', '', 1),
(116, 432, 14, 'neil george', 'corneliusgeorge04@gmail.com', 'test1234', 0, 'f839a97b93e1585adb03a050fecf57e18512cbf9ecf426d2e51f771d3453afd8', 'User1.png', 'coach', 5, 'Active', '2014-09-20 09:26:35', '2014-09-25 13:44:49', 1, '6105872611', '', '', '', '', '', '', '', '', 1),
(117, 405, 11, 'Rajeev Uppala', 'rajeev.uppala@gmail.com', '123456789', 3, 'APA91bEyuixBuDGLbyzQfICziwS51T5T6dVcBBGj1c7Mohca2wW6s6PTsEdgG3zWEgYE5VvgzVrDfnCHn8Iob0JdoxiFt_m1zy-9fUU2WRDndWeaMwJJmr15pScGSwyO8tqGxgqKc_BC', 'teammembers_14055831291439469124.png', 'hi testing this by rajeev', 151, 'Active', '2014-09-20 09:51:15', '2015-08-04 06:59:49', 1, '', 'test', '', '', '', '', '', '', '', 0),
(130, 532, 67, 'pankaj', 'pankaj.oditi@gmail.com', 'test1234', 0, 'APA91bGGuPu_K1lXaSqJpNNyGu8i_zLBmh6T5HtXATFNNGqivFINYnx6vkBqM-hqiEfefS2dhH5Bm-To752xXVRbIxQqeNtWB3AcVOjAu6AI6Qi1ePLkLP0n-zlBsYhjQhI2nchyHe3LXRBg0xw_wLOiUhzNICKO3YudqpsmOlfC8PDk3_1d9ac', 'teammembers_1425985032928243843.png', 'Hello', 0, 'Active', '2015-03-10 06:56:50', '2015-03-10 06:57:12', 1, '1234567890', '', '', '', '', '', '', '', '', 1),
(120, 423, 13, 'Dilan', 'dilan@webappclouds.com', '123456789', 0, 'f839a97b93e1585adb03a050fecf57e18512cbf9ecf426d2e51f771d3453afd8', 'User1.png', 'test email', 0, 'Active', '2014-09-20 10:02:15', '2014-09-20 10:04:33', 1, '', '', '', '', '', '', '', '', '', 1),
(124, 441, 15, 'Dilantha desilva', 'dilan@webappclouds.com', '1234567890', 0, 'f839a97b93e1585adb03a050fecf57e18512cbf9ecf426d2e51f771d3453afd8', 'User1.png', 'test', 0, 'Active', '2014-10-10 12:23:35', '2014-10-10 12:23:35', 1, '', '', '', '', '', '', '', '', '', 1),
(125, 495, 49, 'parker george', 'adnerb0353@aol.com', '1234567tm', 0, '', 'User1.png', 'team member', 0, 'Active', '2015-03-04 08:39:43', '2015-03-04 08:39:43', 1, '', '', '', '', '', '', '', '', '', 1),
(126, 495, 49, 'brandon george', 'brokklee@aol.com', '666777tm', 0, '', 'User1.png', 'teammember', 0, 'Active', '2015-03-04 08:41:22', '2015-03-04 08:41:22', 1, '6105872611', 'neil', '', '', '', '', '', '', '', 1),
(127, 495, 49, 'shady mcKoy', 'bugginhome@aol.com', '332719tm', 0, 'APA91bGrllnaeC2W9jBLOClv44zAyNFb5u3YKP9ei0Vg0zAnM29BLNnY0HUp4IFsWPyBfnaWCnASoOs9v6GzoCUcANDscLDR6F8XJBXrsgP4gWAWNoynFvRHXwlttFKr1dOmMy4PI0Nr', 'User1.png', 'team member', 0, 'Active', '2015-03-04 08:43:23', '2015-03-04 08:43:23', 1, '', '', '', '', '', '', '', '', '', 1),
(128, 495, 49, 'josh gordon', 'hamburgcoachneil@aol.com', '654321tm', 0, 'acb59a7edb9267ce5a9dffe1f498333be8626463b44a14c56ded053db059fad2', 'User1.png', 'team member', 0, 'Active', '2015-03-04 08:44:50', '2015-03-04 08:44:50', 1, '', '', '', '', '', '', '', '', '', 1),
(131, 532, 67, 'barjinder singh', 'barjinder.oditi@gmail.com', 'test1234', 0, '', 'teammembers_19332996441425986253.png', 'test', 0, 'Active', '2015-03-10 07:15:27', '2015-03-10 07:15:27', 1, '9912397035', '', '', '', '', '', '', '', '', 1),
(132, 522, 56, 'Tammy Roberts', 'troberts@battlinminers.com', 'ashley', 0, 'APA91bGqJS1e4iTRYGNYT0X6GqjPAHnmt88cDujHDjU2VUap1IA5VgnOVQm9xUEbALzi40meOyNvsev8o4f_xYilOQJclvQ78D1Xf1_UdUxyPImIm2QEYJuRoy8HvCYTGBHuA71Z4TZh13dlSyAygeGNVKvcPXEMFQ', 'User1.png', 'Ashley''s Mom', 0, 'Active', '2015-03-31 20:07:07', '2015-03-31 20:07:07', 1, '570-294-6720', '', '', '', '', '', '', '', '', 1),
(133, 522, 56, 'Jodi Adams', 'jmk827@hotmail.com', 'liberty', 0, '1f8cf5f95a409aa41752776e44890305fa15813cc2341cb6a95b354c73dd1e3f', 'User1.png', 'Abby''s Mom', 0, 'Active', '2015-04-07 21:41:08', '2015-04-07 21:41:08', 1, '5706409015', '', '', '', '', '', '', '', '', 1),
(134, 522, 56, 'Rob Wentz', 'robwentzjr@verizon.net', 'kaylei05', 0, '784925da2035ba9231f456a9cc927d9aba83fc0445e772279b3067dba920f3a1', 'User1.png', 'Kaylei''s Dad', 0, 'Active', '2015-04-07 21:43:29', '2015-04-07 21:43:29', 1, '5706170007', '', '', '', '', '', '', '', '', 1),
(203, 773, 113, 'Brandon George', 'Cbgeorge6@gmail.com', 'branfon30', 0, '', '', 'Fresman', 0, 'Active', '2015-07-28 04:43:05', '2015-07-28 04:43:05', 1, '6106210426', '', '', '', '', '', '', '', '', 0),
(213, 897, 130, 'dilan', 'dilan@webappclouds.com', 'hello', 0, 'f839a97b93e1585adb03a050fecf57e18512cbf9ecf426d2e51f771d3453afd8', 'User1.png', 'hello', 2, 'Active', '2015-08-18 13:25:20', '2015-08-18 13:25:20', 1, '', '', '', '', '', '', '', '', '', 0),
(136, 773, 113, 'Stacie george', 'Bugginhome@aol.com', 'saints001', 0, 'APA91bGrllnaeC2W9jBLOClv44zAyNFb5u3YKP9ei0Vg0zAnM29BLNnY0HUp4IFsWPyBfnaWCnASoOs9v6GzoCUcANDscLDR6F8XJBXrsgP4gWAWNoynFvRHXwlttFKr1dOmMy4PI0Nr', 'teammembers_1429023215626627675.png', 'Midget varsity', 0, 'Active', '2015-04-14 10:53:35', '2015-04-14 10:53:35', 1, '6105872612', 'Neil george', '', '', '', '', '', '', '', 1),
(137, 522, 56, 'Tammy Wagner', 't11jtlaw@yahoo.com', 'Toodles1', 0, '', 'User1.png', 'Katlei Wentz''s Mom', 0, 'Active', '2015-04-20 17:19:11', '2015-04-20 17:19:11', 1, '5705277404', '', '', '', '', '', '', '', '', 1),
(138, 522, 56, 'Joel and Kathy Motuk', 'jmotuk@battlinminers.com', 'Grace113', 0, '', 'User1.png', 'Grace''s parents', 0, 'Active', '2015-04-20 17:21:49', '2015-04-20 17:21:49', 1, '5705271689', '', '', '', '', '', '', '', '', 1),
(139, 522, 56, 'Louise Horan', 'lilredhouse30@yahoo.com', 'mandmhoran', 0, '', 'User1.png', 'Meg''s Mom', 0, 'Active', '2015-04-20 17:23:08', '2015-04-20 17:23:08', 1, '5706405711', '', '', '', '', '', '', '', '', 1),
(140, 522, 56, 'Diane Herb', 'wasnme77@gmail.com', 'Alyssa11', 0, '', 'User1.png', 'Alyssa''s Mom', 0, 'Active', '2015-04-20 17:24:32', '2015-04-20 17:24:32', 1, '5707893127', '', '', '', '', '', '', '', '', 1),
(141, 522, 56, 'Melissa Zula', 'lissaz3305@aol.com', 'snoopy', 0, 'APA91bFwL7HC8pYNFULlTNO60H-1Rru4NuiLtMz-1p5D2kpXQaY-YD04gd1O0fl0aNI0r1qDKZNkcEft7j2f9sLOXqduzS9gnv14gm43cV6zvhbKeg8VjjKDuv3yeJCLL35T6hvnzcPwUGp-w2yR2yRgQF1slIvNVQ', 'User1.png', 'Lauren''s Mom', 0, 'Active', '2015-04-20 17:26:07', '2015-04-20 17:26:07', 1, '5704490107', '', '', '', '', '', '', '', '', 1),
(142, 522, 56, 'Rich Zula', 'rmrz3305@aol.com', 'snoopy', 0, '', 'User1.png', 'Lauren''s Dad', 0, 'Active', '2015-04-20 17:27:09', '2015-04-20 17:27:09', 1, '5704493563', '', '', '', '', '', '', '', '', 1),
(143, 522, 56, 'Colleen Hertz', 'pritzie@verizon.net', 'colleen10', 0, '', 'User1.png', 'Lauren''s Mom', 0, 'Active', '2015-04-20 17:28:23', '2015-04-20 17:28:23', 1, '5705736450', '', '', '', '', '', '', '', '', 1),
(144, 522, 56, 'Rachel Huntzinger', 'huntzinger1@verizon.net', 'beer4us', 0, '', 'User1.png', 'Emily''s Mom', 0, 'Active', '2015-04-20 17:29:39', '2015-04-20 17:29:39', 1, '5703911946', '', '', '', '', '', '', '', '', 1),
(145, 522, 56, 'Charlie Roberts', 'charoberts.64@gmail.com', 'ashley02', 0, '90:B6:86:5D:25:27', 'User1.png', 'Ashley''s Dad', 0, 'Active', '2015-04-20 17:31:43', '2015-04-22 22:29:33', 1, '5706176464', '', '', '', '', '', '', '', '', 1),
(188, 405, 11, 'syam', 'pydahakarsh8@gmail.com', '123456789', 0, '', 'teammembers_1442580580849855188.png', 'hi', 0, 'Active', '2015-07-27 08:41:02', '2015-09-18 08:49:40', 1, '9999999999', '', '', '', '', '', '', '', '', 0),
(214, 897, 130, 'test', 'test@gmail.com', '12345', 0, 'APA91bHZE7OrU2UR6GFeTWQXpkF2CLUOgG_wnvH7n1W41uZow924W-QuZdyDhtiq88u28qdnwKYQIKkkFCbhJ5geuKFoaWqxejxbpFlTFO58J87DB9GZkJ3_jAaNZJ4VpZhwCWsjtWYB', '', 'test', 0, 'Active', '2015-08-18 14:04:36', '2015-08-18 14:04:36', 1, '9999999999', '', '', '', '', '', '', '', '', 0),
(216, 897, 130, 'Terry', 'Terrym@Lordsandladiessalons.com', 'test123', 8, '5757e427af62fd01aa3c650bafc9db6a4175c1c845931c6f7c415100417f253f', '', 'Player', 4, 'Active', '2015-08-18 15:01:42', '2015-08-18 15:01:42', 1, '6102071651', '', '', '', '', '', '', '', '', 0),
(215, 897, 130, 'Test2', 'test2@gmail.com', '12345', 0, '', '', 'test', 0, 'Active', '2015-08-18 14:06:10', '2015-08-18 14:06:10', 1, '9999999999', '', '', '', '', '', '', '', '', 0),
(211, 405, 11, 'Srinu', 'srinivasulu.1216@gmail.com', 'srinu', 17, 'c185f8eff9973e2b7031e525f6122e8f085cfa7bd34d86e57f69f569e484e2fb', 'teammembers_1439198598302370278.png', 'test', 2, 'Active', '2015-08-10 02:16:58', '2015-08-10 05:23:18', 1, '123456789', '', '', '', '', '', '', '', '', 0),
(212, 897, 130, 'Terencemckim@yahoo.com', 'Terencemckim@yahoo.com', 'hellohello', 0, '', 'User1.png', 'ddddd', 0, 'Active', '2015-08-18 13:24:10', '2015-08-18 13:24:47', 1, '', '', '', '', '', '', '', '', '', 0),
(217, 504, 50, 'Joie', 'Joiemckim@gmail.com', 'addy150', 0, '', '', 'Soccer player', 0, 'Active', '2015-09-18 20:54:47', '2015-09-18 20:54:47', 1, '6108230914', '', '', '', '', '', '', '', '', 0),
(218, 504, 50, 'Joie', 'Jolemckim@gmail.com', 'addy150', 0, '', '', 'Cheerleader', 0, 'Active', '2015-09-18 20:55:58', '2015-09-18 20:55:58', 1, '6108230914', '', '', '', '', '', '', '', '', 0),
(219, 7, 1, 'TestA', 'emaila@email.com', 'passworda', 13, '9b1d6fbbf14fcc24', '', '', 0, 'Active', '2015-09-22 00:00:00', '2015-09-22 00:00:00', 0, '', '', '', '', '', '', '', '', '', 0),
(220, 405, 11, 'Test', 'Test', 'test', 0, '', '', 'Test', 0, 'Active', '2015-10-18 06:31:33', '2015-10-18 06:31:33', 1, '999999999', '', '', '', '', '', '', '', '', 0),
(221, 405, 11, 'Test', 'Trst@gmail.com', 'test', 0, '', '', 'Test', 0, 'Active', '2015-10-19 03:09:49', '2015-10-19 03:09:49', 1, '9999999999', '', '', '', '', '', '', '', '', 0),
(222, 405, 11, 'Test', 'slivajasmineg@yahoo.com', 'assfghjkl', 0, '', '', 'Test', 0, 'Active', '2015-10-19 03:12:14', '2015-10-19 03:12:14', 1, '8888888888', '', '', '', '', '', '', '', '', 0),
(223, 405, 11, 'test4', 'silvajasmineg@yahoo.com', '999999999', 0, '', '', 'test', 0, 'Active', '2015-10-19 03:17:41', '2015-10-19 03:17:41', 1, '8888888888', '', '', '', '', '', '', '', '', 0),
(224, 981, 142, 'Test1', 'test1@gmail.com', 'test', 0, '', '', 'test', 0, 'Active', '2015-10-21 01:46:33', '2015-10-21 01:46:33', 1, '9999999999', '', '', '', '', '', '', '', '', 0),
(225, 405, 11, 'kaler', 'aman@webappclouds.com', 'aman', 0, '', '', 'A test\n', 0, 'Active', '2015-10-21 06:42:27', '2015-10-21 06:42:27', 1, '9855755075', '', '', '', '', '', '', '', '', 0),
(228, 1058, 153, 'kranthi', 'kranthiethan@gmail.com', 'kranthi', 31, 'c185f8eff9973e2b7031e525f6122e8f085cfa7bd34d86e57f69f569e484e2fb', 'User1.png', 'kranthi', 89, 'Active', '2015-10-28 12:00:20', '2015-10-29 07:22:00', 1, '', '', '', '', '', '', '', '', '', 0),
(229, 1058, 153, 'Rajeev', 'rajeev.uppala@gmail.com', 'rajeev', 3, 'APA91bEyuixBuDGLbyzQfICziwS51T5T6dVcBBGj1c7Mohca2wW6s6PTsEdgG3zWEgYE5VvgzVrDfnCHn8Iob0JdoxiFt_m1zy-9fUU2WRDndWeaMwJJmr15pScGSwyO8tqGxgqKc_BC', 'User1.png', 'rajeev', 70, 'Active', '2015-10-28 12:00:57', '2015-10-29 07:21:45', 1, '', '', '', '', '', '', '', '', '', 0),
(230, 1065, 154, 'Rajeev', 'Rajeev@webappclouds.com', '1234', 27, '0cd5b5f6f00c84af9f8b45f2ac9308dab060f1d5c7c8ccdb615842fb63c69d78', '', 'Test 23', 1, 'Active', '2015-10-29 05:46:29', '2015-10-29 05:46:29', 1, '2155551212', '', '', '', '', '', '', '', '', 0),
(232, 1058, 153, 'Prakash', 'Prakash@webappclouds.com', 'prakash', 37, 'APA91bGwKrQVXjzB3Qo33QVMEOiWDVQ92TTwkKktMIms_D9uPjiDoXoMhEDUKo58yvS2qOXrGGnfjKWYXlP0r8wG5ir2YRf7eo9W-sv1cvs0IXmsnM3irGuQlj6yC820m6Ui-qhgy7ea', '', 'Prakash seo', 2, 'Active', '2015-10-29 07:53:15', '2015-10-29 07:53:15', 1, '8528528520', '', '', '', '', '', '', '', '', 0),
(233, 583, 92, 'Coach Ken', 'ken.thomason67@gmail.com', 'wilson2015', 0, '', '', 'HS Wrestling', 0, 'Active', '2015-11-10 13:47:14', '2015-11-10 13:47:14', 1, '(610) 507-2549', '', '', '', '', '', '', '', '', 0),
(234, 1128, 163, 'Brandon George', 'Brokklee@aol.com', 'brandon30', 0, '', '', '', 0, 'Active', '2015-11-16 09:36:45', '2015-11-16 09:36:45', 1, '6106210426', '', '', '', '', '', '', '', '', 0),
(235, 1128, 163, 'Stacie George', 'Bugginhome@aol.com', 'chopper27', 34, 'APA91bGrllnaeC2W9jBLOClv44zAyNFb5u3YKP9ei0Vg0zAnM29BLNnY0HUp4IFsWPyBfnaWCnASoOs9v6GzoCUcANDscLDR6F8XJBXrsgP4gWAWNoynFvRHXwlttFKr1dOmMy4PI0Nr', '', 'Team mom', 6, 'Active', '2015-11-16 09:37:52', '2015-11-16 09:37:52', 1, '6105872612', '', '', '', '', '', '', '', '', 0),
(236, 1065, 154, 'Test', 'It@Webappclouds.com', '1234', 0, '', '', 'Test', 0, 'Active', '2015-11-17 02:43:01', '2015-11-17 02:43:01', 1, '2672074190', '', '', '', '', '', '', '', '', 0),
(271, 1065, 154, 'asdf', 'asdf@gmail.com', '123456', 0, '', '', 'asd', 0, 'Active', '2016-03-01 03:48:07', '2016-03-01 03:48:07', 1, '5555555555', '', '', '', '', '', '', '', '', 0),
(272, 1065, 154, 'syam2', 'asd@gmail.com', 'asdf', 0, '', '', 'df', 0, 'Active', '2016-03-01 04:04:39', '2016-03-01 04:04:39', 1, '1234567899', '', '', '', '', '', '', '', '', 0),
(238, 1135, 164, 'Stacie George', 'Bugginhome@aol.com', 'vhopper27', 34, 'APA91bGrllnaeC2W9jBLOClv44zAyNFb5u3YKP9ei0Vg0zAnM29BLNnY0HUp4IFsWPyBfnaWCnASoOs9v6GzoCUcANDscLDR6F8XJBXrsgP4gWAWNoynFvRHXwlttFKr1dOmMy4PI0Nr', '', 'Team mom', 4, 'Active', '2015-11-17 03:21:41', '2015-11-17 03:21:41', 1, '6105872612', '', '', '', '', '', '', '', '', 0),
(240, 1135, 164, 'Brandon George', 'cbgeorge6@gmail.com', 'brandon30', 0, '', '', 'Player', 0, 'Active', '2015-11-17 06:07:16', '2015-11-17 06:07:16', 1, '6106210426', '', '', '', '', '', '', '', '', 0),
(241, 1135, 164, 'Dillan DeS', 'Dilan@Webappclouds.com', 'sporty1234', 0, '', '', '', 0, 'Active', '2015-11-17 09:41:02', '2015-11-17 09:41:02', 1, '2672074190', '', '', '', '', '', '', '', '', 0),
(243, 1149, 166, 'test', 'amankaler0@gmail.com', 'test1234', 19, 'APA91bH_Dzz7Rd9hBecaD0rBCk__J6LNMcNG4WC87cLsZEbqQm55YREgbrDAMB4JdDc_xSopYlx-5T2XvUD9MTcHvxfasBNcoI7Mn4qVe8w4hoI9LoaItYfuhAYvR4Nt427CWajz8NqY', '', 'test app', 1, 'Active', '2015-11-25 00:38:42', '2015-11-25 00:38:42', 1, '1234567890', '', '', '', '', '', '', '', '', 0),
(244, 1149, 166, 'abc', 'test@tester.com', 'test1234', 0, '', '', 'abc', 0, 'Active', '2015-11-25 00:39:53', '2015-11-25 00:39:53', 1, '1234567890', '', '', '', '', '', '', '', '', 0),
(245, 1163, 168, 'SoS Elite (Seven on Seven)', 'davejones@premierrents.com', 'premier001', 0, '', '', 'Berks County Elite 7 on 7', 0, 'Active', '2015-12-23 00:23:32', '2016-01-04 15:49:02', 1, '4847690454', '', '', '', '', '', '', '', '', 0),
(246, 1163, 168, 'Neil George', 'hamburgcoachneil@aol.com', 'george', 7, 'acb59a7edb9267ce5a9dffe1f498333be8626463b44a14c56ded053db059fad2', '', 'Brandons dad', 11, 'Active', '2015-12-28 12:22:21', '2015-12-28 12:22:21', 1, '6105872611', '', '', '', '', '', '', '', '', 0),
(247, 1163, 168, 'David Jones', 'djones0822@yahoo.com', 'jones', 0, '', '', 'Player', 0, 'Active', '2015-12-28 12:26:42', '2015-12-28 12:26:42', 1, '4846509171', '', '', '', '', '', '', '', '', 0),
(248, 1163, 168, 'David Jones', 'usskc@yahoo.com', 'jones', 41, 'APA91bF7Pj608VZZl6bfSe766knDVyr3JT7BB7KNHC_aS2HVJhjUL9yRUTHjgAag75X3XMbds2eweWuMuS_GduI0E7CTxgqTQfDc_3FiaAgTzgLi5iNSWYCU5Ba_3xIsaRkd4siA1Y4r', '', 'David dad', 2, 'Active', '2015-12-28 12:27:31', '2015-12-28 12:27:31', 1, '4847690454', '', '', '', '', '', '', '', '', 0),
(249, 974, 141, 'test', 'test@email.com', 'test123', 0, '', 'User1.png', 'test', 0, 'Active', '2015-12-28 12:41:51', '2015-12-28 12:58:22', 1, '', '', '', '', '', '', '', '', '', 0),
(250, 1163, 168, 'James Martinez', 'nfanortheast@hotmail.com', 'martinez', 0, '', 'User1.png', 'Head Coach', 0, 'Active', '2016-01-04 15:40:24', '2016-01-04 15:40:24', 1, '', '', '', '', '', '', '', '', '', 0),
(251, 1163, 168, 'Marcus Wilson', 'marcus7wilson@outlook.com', 'wilson', 0, '', 'User1.png', 'player', 0, 'Active', '2016-01-04 15:41:29', '2016-01-04 15:41:59', 1, '', '', '', '', '', '', '', '', '', 0),
(252, 1163, 168, 'John Paulik', 'wjpaulik@yahoo.com', 'paulik', 0, '', 'User1.png', 'player', 0, 'Active', '2016-01-04 15:42:49', '2016-01-04 15:42:49', 1, '484-599-0115', '', '', '', '', '', '', '', '', 0),
(253, 1163, 168, 'Terry Derr', 'lordderrjr@aol.com', 'derr', 0, '', 'User1.png', 'Derr''s dad', 0, 'Active', '2016-01-04 15:43:42', '2016-01-04 15:43:42', 1, '610-334-5510', '', '', '', '', '', '', '', '', 0),
(254, 1163, 168, 'Cooper Lutz', 'tifflutz@hotmail.com', 'lutz', 0, '', 'User1.png', 'Cooper''s mom', 0, 'Active', '2016-01-04 15:44:43', '2016-01-04 15:44:43', 1, '610-779-6994', '', '', '', '', '', '', '', '', 0),
(255, 1163, 168, 'Matthew Hagelbarger', 'jmhagel@ptd.net', 'hagelbarger', 0, '', 'User1.png', 'Matt''s dad', 0, 'Active', '2016-01-04 15:45:57', '2016-01-04 15:45:57', 1, '610-779-6129', '', '', '', '', '', '', '', '', 0),
(256, 1163, 168, 'Tre Dabney', 'dabbs23@outlook.com', 'dabney', 0, '', 'User1.png', 'player', 0, 'Active', '2016-01-04 15:47:18', '2016-01-04 15:47:18', 1, '484-219-0149', '', '', '', '', '', '', '', '', 0),
(257, 1163, 168, 'Angel Arias', 'sweetdanger_1883@hotmail.com', 'arias', 0, '', 'User1.png', 'Angel''s mom', 0, 'Active', '2016-01-04 15:48:33', '2016-01-04 15:48:33', 1, '484-794-1795', '', '', '', '', '', '', '', '', 0),
(258, 1093, 158, 'test1', 'test1@gmail.com', 'test', 0, '', '', 'test', 0, 'Active', '2016-02-25 04:28:36', '2016-02-25 04:28:36', 1, '9999999999', '', '', '', '', '', '', '', '', 0),
(259, 1093, 158, 'team2', 'team2@gmail.com', 'team2', 0, '', '', 'test', 0, 'Active', '2016-02-25 04:29:26', '2016-02-25 04:29:26', 1, '8888888888', '', '', '', '', '', '', '', '', 0),
(261, 1093, 158, 'test6', 'test6@gmail.com', 'test6', 0, '', '', 'test', 0, 'Active', '2016-02-25 05:12:13', '2016-02-25 05:12:13', 1, '6666666666', '', '', '', '', '', '', '', '', 0),
(270, 1065, 154, 'syam1', 'syam@gmail.com', 'asdf', 0, '', '', 'hi', 0, 'Active', '2016-03-01 03:42:25', '2016-03-01 03:42:25', 1, '6666666666', '', '', '', '', '', '', '', '', 0),
(264, 1065, 154, 'test2', 'test2@gmail.com', 'test1', 0, '', '', 'test', 0, 'Active', '2016-03-01 01:48:21', '2016-03-01 01:48:21', 1, '9999999999', '', '', '', '', '', '', '', '', 0),
(265, 1065, 154, 'team3', 'team3@gmail.com', 'team3', 0, '', '', 'test', 0, 'Active', '2016-03-01 01:51:44', '2016-03-01 01:51:44', 1, '7777777777', '', '', '', '', '', '', '', '', 0),
(266, 1065, 154, 'test4', 'test4@gmail.com', '222222', 0, '', '', 'test', 0, 'Active', '2016-03-01 01:53:40', '2016-03-01 01:53:40', 1, '2222222222', '', '', '', '', '', '', '', '', 0),
(267, 1065, 154, 'test', 'test@gmail.com', '1234', 0, '', '', 'test', 0, 'Active', '2016-03-01 01:57:31', '2016-03-01 01:57:31', 1, '6161', '', '', '', '', '', '', '', '', 0),
(268, 1065, 154, 'sr', 'sr@gmail.com', '1234', 0, '', '', 'sf', 0, 'Active', '2016-03-01 02:03:01', '2016-03-01 02:03:01', 1, '12346689', '', '', '', '', '', '', '', '', 0),
(269, 1065, 154, 'syam', 'testsyam@gmail.com', 'vgy', 0, '', '', 'buffet ', 0, 'Active', '2016-03-01 02:35:30', '2016-03-01 02:35:30', 1, '8526398740', '', '', '', '', '', '', '', '', 0),
(273, 1065, 154, 'kranthi', 'testkra@gmail.com', 'bhu', 0, '', '', 'buu', 0, 'Active', '2016-03-01 04:11:09', '2016-03-01 04:11:09', 1, '9639639639', '', '', '', '', '', '', '', '', 0),
(274, 1065, 154, 'krishna', 'k@gmauk.com', 'k', 0, '', '', 'k', 0, 'Active', '2016-03-01 04:12:35', '2016-03-01 04:12:35', 1, '9639879639', '', '', '', '', '', '', '', '', 0),
(275, 1065, 154, 'qwert', 'qwe@gmail.com', '44444', 0, '', '', 'as', 0, 'Active', '2016-03-01 04:15:02', '2016-03-01 04:15:02', 1, '2222222222', '', '', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `plus_templates_settings`
--

CREATE TABLE IF NOT EXISTS `plus_templates_settings` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `template` varchar(20) NOT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_training`
--

CREATE TABLE IF NOT EXISTS `plus_training` (
  `training_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `training_title` varchar(200) NOT NULL,
  `training_url` text NOT NULL,
  `training_status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `training_create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `training_update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `training_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`training_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `plus_training`
--

INSERT INTO `plus_training` (`training_id`, `module_id`, `salon_id`, `training_title`, `training_url`, `training_status`, `training_create_date`, `training_update_date`, `training_order`) VALUES
(1, 382, 1, 'Bradley Wilson - Wrestling March 8, 2014', 'https://www.youtube.com/watch?v=PcFRCtgQyho', 'Active', '2014-04-28 04:16:35', '2014-04-28 07:10:21', 2),
(8, 382, 1, 'PHP', 'https://www.google.com/', 'Active', '2014-08-08 01:51:48', '2014-08-08 01:52:24', 1),
(3, 382, 1, 'Brock Wilson Wrestling at Fargo 2013', 'https://www.youtube.com/watch?v=K5gU4Qn4qx4', 'Active', '2014-04-28 07:11:01', '2014-04-28 07:11:15', 3),
(6, 382, 1, 'video2', 'https://www.youtube.com/watch?v=HYhI4CD6Wf8', 'Active', '2014-08-07 00:48:42', '2014-08-09 07:55:09', 1),
(9, 382, 1, 'new', 'http://google.co.in/', 'Active', '2014-08-11 06:32:28', '2014-08-13 05:46:34', 1),
(11, 443, 15, 'video2', 'https://www.youtube.com/watch?v=HYhI4CD6Wf8', 'Active', '2014-10-16 06:16:31', '2014-10-16 06:16:31', 1),
(12, 497, 49, 'training', 'http://youtu.be/c2yr28vEGNA', 'Active', '2015-03-04 10:07:36', '2015-03-04 10:07:36', 1),
(23, 544, 77, 'test', 'https://www.youtube.com/watch?v=ZsCC2cFqALU', 'Active', '2015-03-25 06:59:58', '2015-03-25 06:59:58', 1),
(22, 407, 11, 'test', 'https://www.youtube.com/watch?v=ZsCC2cFqALU', 'Active', '2015-03-11 06:48:38', '2015-03-11 06:48:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `plus_user`
--

CREATE TABLE IF NOT EXISTS `plus_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `user_first_name` varchar(100) NOT NULL,
  `user_last_name` varchar(100) NOT NULL,
  `user_email_id` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `cometchat_userId` int(11) NOT NULL,
  `user_phone` varchar(14) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_zip` varchar(8) NOT NULL,
  `user_city` varchar(100) NOT NULL,
  `user_state` varchar(100) NOT NULL,
  `user_is_admin` enum('1','0') NOT NULL,
  `user_status` enum('Active','Inactive') NOT NULL,
  `user_create_date` datetime NOT NULL,
  `user_update_date` datetime NOT NULL,
  `user_last_login` datetime NOT NULL,
  `user_position` enum('Owner','Employee') NOT NULL DEFAULT 'Employee',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=352 ;

--
-- Dumping data for table `plus_user`
--

INSERT INTO `plus_user` (`user_id`, `salon_id`, `user_first_name`, `user_last_name`, `user_email_id`, `user_password`, `cometchat_userId`, `user_phone`, `user_address`, `user_zip`, `user_city`, `user_state`, `user_is_admin`, `user_status`, `user_create_date`, `user_update_date`, `user_last_login`, `user_position`) VALUES
(191, 0, 'admin', 'admin', 'admin@sportyfox.com', 'test@12345', 0, '9646977333', 'mohali', '123456', 'mohali', 'IN', '1', 'Active', '2014-08-06 06:17:01', '2015-11-01 00:05:35', '2016-02-29 04:56:40', 'Employee'),
(196, 0, 'Neil', 'George', 'hamburgcoachneil@aol.com', 'test1234', 0, '', '', '', '', '', '1', 'Active', '2014-09-11 09:49:22', '2014-09-11 09:49:22', '2016-02-29 05:21:07', 'Employee'),
(258, 74, 'Saints', 'Saints', 'Hamburgvoachneil@aol.com', 'boogie0607', 0, '', '', '', '', '', '1', '', '2015-03-24 02:26:35', '2015-03-24 02:26:35', '2016-02-01 22:47:35', 'Employee'),
(265, 81, 'Demo23', 'Demo23', 'pankaj.oditi@gmail.com', 'oditi169', 0, '', '', '', '', '', '1', 'Active', '2015-03-25 08:14:24', '2015-03-25 08:14:24', '2016-02-01 22:47:35', 'Employee'),
(270, 86, 'PA Swag SwagBacks 7v7', 'PA Swag SwagBacks 7v7', 'sherri@paswag.net', 'Leyah21', 0, '', '', '', '', '', '1', 'Active', '2015-03-26 13:57:49', '2015-03-26 13:57:49', '2016-02-01 22:47:35', 'Employee'),
(274, 92, 'Wilson Wrestling', 'Wilson Wrestling', 'terencemckim@yahoo.com', 'Addy150', 12, '', '', '', '', '', '1', 'Active', '2015-03-30 14:08:36', '2015-03-30 14:08:36', '2016-02-01 22:47:35', 'Employee'),
(275, 93, 'Mnersville Little League', 'Mnersville Little League', 'chasroberts@verizon.net', 'battlinminers', 0, '', '', '', '', '', '1', 'Active', '2015-03-30 16:55:26', '2015-03-30 16:55:26', '2016-02-01 22:47:35', 'Employee'),
(276, 94, 'Holman Hurricanes 14U Elite', 'Holman Hurricanes 14U Elite', 'davejones@premierrents.com', 'baseball', 0, '', '', '', '', '', '1', 'Active', '2015-03-31 12:33:27', '2015-03-31 12:33:27', '2016-02-01 22:47:35', 'Employee'),
(291, 111, 'saints', 'saints', 'hamburgcoachneil@aol.com', 'boogie0607', 7, '', '', '', '', '', '1', '', '2015-04-08 12:14:08', '2015-04-08 12:14:08', '2016-02-29 05:21:07', 'Employee'),
(292, 112, 'Saints', 'Saints', 'Hamburgcoachneil@aol.com', 'Zooba1906', 0, '', '', '', '', '', '1', 'Active', '2015-04-14 10:46:16', '2015-04-14 10:46:16', '2016-02-29 05:21:07', 'Employee'),
(293, 113, 'Saints', 'Saints', 'Hamburgcoachneil@aol.com', 'hello@123', 0, '', '', '', '', '', '1', 'Active', '2015-04-14 10:46:40', '2015-07-24 22:55:29', '2016-02-29 05:21:07', 'Employee'),
(295, 115, 'Berks Catholic', 'Berks Catholic', 'ruamazed@yahoo.com', 'Passw0rd1', 0, '', '', '', '', '', '1', 'Active', '2015-06-19 17:22:44', '2015-06-19 17:22:44', '2016-02-01 22:47:35', 'Employee'),
(296, 116, 'Hello', 'Hello', 'Terencemckim@yahoo.com', 'hellohello', 0, '', '', '', '', '', '1', 'Active', '2015-07-17 09:36:19', '2015-08-18 13:18:59', '2016-02-01 22:47:35', 'Employee'),
(306, 128, 'Neil', 'George', 'Hamburgcoachneil@aol.com', 'football19', 7, '', '', '', '', '', '', 'Inactive', '2015-08-14 15:28:49', '2015-08-14 15:28:49', '2016-02-29 05:21:07', 'Owner'),
(310, 130, 'd', 'd', 'dd@yahoo.com', 'hellohello@1', 6, '11111111', '1906 riverview drive', '19102', '111', '11', '0', 'Active', '2015-08-18 13:09:21', '2015-08-18 13:38:03', '2016-02-01 22:47:35', 'Owner'),
(312, 132, 'Terry', 'Mckim', 'Terencemckim@yahoo.com', 'addy150', 0, '', '', '', '', '', '', 'Inactive', '2015-09-18 20:53:04', '2015-09-18 20:53:04', '2016-02-01 22:47:35', 'Owner'),
(313, 133, 'Neil', 'Hamilton', 'hdreaction100@gmail.com', 'GYFL2015', 9, '', '', '', '', '', '0', 'Active', '2015-09-30 13:21:42', '2015-10-27 20:03:02', '2016-02-01 22:47:35', 'Owner'),
(314, 134, 'Howard', 'Robins', 'howrobjr@aol.com', 'Jus4u2nv!', 16, '', '', '', '', '', '0', 'Active', '2015-10-14 00:18:51', '2015-10-27 20:03:59', '2016-02-01 22:47:35', 'Owner'),
(319, 139, 'Dan', 'Francisco', 'dfrancisco@mcsomo.com', 'FR@1983dt!', 0, '', '', '', '', '', '0', 'Active', '2015-10-20 12:31:12', '2015-10-27 20:13:10', '2016-02-01 22:47:35', 'Owner'),
(321, 141, 'Daniel', 'Mosquera', 'bc.outsiderz@gmail.com', 'dan2121315', 22, '', '', '', '', '', '0', 'Active', '2015-10-20 16:31:20', '2015-10-21 01:42:45', '2016-02-01 22:47:35', 'Owner'),
(323, 143, 'Terry', 'Mckim', 'Terencemckim@yahoo.com', 'addy150', 0, '', '', '', '', '', '', 'Active', '2015-10-21 14:39:53', '2015-10-21 14:39:53', '2016-02-01 22:47:35', 'Owner'),
(324, 144, 'Dane', 'Miller', 'Danemichael.miller@gmail.com', 'hurricaneandanton', 24, '', '', '', '', '', '', 'Active', '2015-10-22 22:29:16', '2015-10-22 22:29:16', '2016-02-01 22:47:35', 'Owner'),
(325, 145, 'Dane', 'Miller', 'Danemichael.miller@gmail.com', 'hurricane', 24, '', '', '', '', '', '', 'Active', '2015-10-23 08:08:56', '2015-10-23 08:08:56', '2016-02-01 22:47:35', 'Owner'),
(326, 146, 'Neil', 'George', 'Brokklee@aol.com', 'brandon30', 0, '', '', '', '', '', '', 'Active', '2015-10-23 08:20:44', '2015-10-23 08:20:44', '2016-02-01 22:47:35', 'Owner'),
(327, 147, 'Dane', 'Miller', 'Dane@garagestrength.com', 'hurricane', 25, '', '', '', '', '', '', 'Active', '2015-10-23 08:21:07', '2015-10-23 08:21:07', '2016-02-01 22:47:35', 'Owner'),
(333, 153, 'test gamer', 'test gamer', 'kranthi@webappclouds.com', 'kranthi123', 29, '', '', '', '', '', '1', 'Active', '2015-10-28 11:59:02', '2015-10-28 11:59:02', '2016-02-01 22:47:35', 'Employee'),
(334, 154, 'Dilan', 'De', 'Dilan@webappclouds.com', 'ibsonibson', 32, '', '', '', '', '', '', 'Active', '2015-10-29 05:39:10', '2015-10-29 05:39:10', '2016-03-01 04:15:01', 'Owner'),
(335, 155, 'jfjdjd', 'hdhdh', 'test@tester.com', 'asdfgh', 0, '', '', '', '', '', '', 'Active', '2015-10-29 06:19:56', '2015-10-29 06:19:56', '2016-02-01 22:47:35', 'Owner'),
(336, 156, 'jfjdjd', 'hdhdh', 'test@tester.com', 'asdfgh', 0, '', '', '', '', '', '', 'Active', '2015-10-29 06:24:05', '2015-10-29 06:24:05', '2016-02-01 22:47:35', 'Owner'),
(337, 157, 'ethan', 'hunt', 'hunter@gmail.com', 'asdfgh', 36, '', '', '', '', '', '', 'Active', '2015-10-29 06:28:14', '2015-10-29 06:28:14', '2016-02-01 22:47:35', 'Owner'),
(338, 158, 'kranthi', 'goli', 'kranthiethan@gmail.com', 'asdfgh', 31, '', '', '', '', '', '', 'Active', '2015-10-29 08:16:46', '2015-10-29 08:16:46', '2016-02-01 22:47:35', 'Owner'),
(339, 159, 'Darryl', 'Johnson', 'coachd375@gmail.com', 'Phillycop2519', 33, '', '', '', '', '', '', 'Active', '2015-11-15 19:16:55', '2015-11-15 19:16:55', '2016-02-01 22:47:35', 'Owner'),
(340, 160, 'Darryl', 'Johnson', 'coachd375@gmail.com', 'Phillycop2519', 0, '', '', '', '', '', '', 'Active', '2015-11-15 19:21:01', '2015-11-15 19:21:01', '2016-02-01 22:47:35', 'Owner'),
(341, 161, 'aman', 'kaler', 'amankaler0@ gmail.com', 'avneet@)!$', 0, '', '', '', '', '', '', 'Active', '2015-11-16 00:47:12', '2015-11-16 00:47:12', '2016-02-01 22:47:35', 'Owner'),
(342, 162, 'aman', 'test', 'amankaler0@gmail.com', 'test123', 19, '', '', '', '', '', '', 'Active', '2015-11-16 00:54:30', '2015-11-16 00:54:30', '2016-02-01 22:47:35', 'Owner'),
(343, 163, 'Neil', 'George', 'Hamburgcoachneil@aol.com', 'brandon0607', 7, '', '', '', '', '', '', 'Active', '2015-11-16 09:34:27', '2015-11-16 09:34:27', '2016-02-29 05:21:07', 'Owner'),
(344, 164, 'Neil ', 'George', 'Corneliusgeorge04@gmail.com', 'football30', 35, '', '', '', '', '', '', 'Active', '2015-11-17 03:18:45', '2015-11-17 03:18:45', '2016-02-01 22:47:35', 'Owner'),
(345, 165, 'Ken', 'Thomason', 'Ken.thomason67@gmail.com', 'bulldogs67', 38, '', '', '', '', '', '', 'Active', '2015-11-17 14:45:48', '2015-11-17 14:45:48', '2016-02-01 22:47:35', 'Owner'),
(346, 166, 'aman', 'test', 'aman@webappclouds.com', 'test1234', 39, '', '', '', '', '', '', 'Active', '2015-11-25 00:22:14', '2015-11-25 00:22:14', '2016-02-01 22:47:35', 'Owner'),
(347, 167, 'kolbaska109', 'Igor', 'kolbaska109@mail.ru', 'madara', 0, '', '', '', '', '', '', 'Active', '2015-12-14 08:36:28', '2015-12-14 08:36:28', '2016-02-01 22:47:35', 'Owner'),
(348, 168, 'David', 'Jones', 'davejones@premierrents.com', 'premier001', 40, '', '', '', '', '', '', 'Active', '2015-12-23 00:20:30', '2015-12-23 00:20:30', '2016-02-01 22:47:35', 'Owner'),
(349, 169, 'Aman', 'Kaler', 'amankaler0@gmail.com', 'test1234', 19, '', '', '', '', '', '', 'Active', '2015-12-31 02:09:10', '2015-12-31 02:09:10', '2016-02-01 22:47:35', 'Owner'),
(350, 170, 'Kevin', 'Shick', 'kevin@knoxweb.com', 'Osmosis.123', 0, '', '', '', '', '', '', 'Active', '2016-01-12 10:03:36', '2016-01-12 10:03:36', '2016-02-03 15:53:10', 'Owner'),
(351, 171, 'saints', 'saints', 'hamburgcoachneil@aol.com', 'boogie0607', 0, '', '', '', '', '', '1', 'Active', '2016-02-01 22:48:57', '2016-02-01 22:48:57', '2016-02-29 05:21:07', 'Employee');

-- --------------------------------------------------------

--
-- Table structure for table `plus_variable`
--

CREATE TABLE IF NOT EXISTS `plus_variable` (
  `variable_id` int(20) NOT NULL AUTO_INCREMENT,
  `salon_id` int(20) NOT NULL,
  `variable_title` varchar(200) NOT NULL,
  `variable_name` varchar(200) NOT NULL,
  `variable_type` varchar(200) NOT NULL DEFAULT 'text',
  `variable_value` text NOT NULL,
  PRIMARY KEY (`variable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plus_video_uploads`
--

CREATE TABLE IF NOT EXISTS `plus_video_uploads` (
  `upload_id` int(11) NOT NULL AUTO_INCREMENT,
  `salon_id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `added_on` datetime NOT NULL,
  PRIMARY KEY (`upload_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `plus_video_uploads`
--

INSERT INTO `plus_video_uploads` (`upload_id`, `salon_id`, `email`, `video_link`, `added_on`) VALUES
(32, 1, 'test@gmail.com', 'https://www.youtube.com/watch?v=lCL3sEtYajY', '2014-10-06 09:07:10'),
(7, 1, 'nithinredd@gmail.com', 'https://www.icloud.com/download/documents/?p=08&t=BAKxGpL2hxQjUw7nCeEBwvoPYM22ao3x9rsF', '2014-07-14 05:35:17'),
(35, 11, 'rajeev.uppala@gmail.com', 'https://www.youtube.com/watch?v=tH5N95qugsg', '2015-03-25 07:36:38'),
(31, 1, 'nithinredd@gmail.com', 'http://youtu.be/MYpb1aFkSkM', '2014-09-29 03:25:38'),
(14, 1, 'test@gmail.com', 'https://www.youtube.com/watch?v=afmJdmxSR2I', '2014-07-25 07:59:36'),
(15, 1, 'nithinredd@gmail.com', 'https://www.youtube.com/watch?v=Do2zeTJDEjo', '2014-07-25 09:05:01'),
(16, 1, 'test@gmail.com', 'https://www.youtube.com/watch?v=YmVmkZchj6E', '2014-07-25 09:09:56'),
(29, 1, 'test@gmail.com', 'https://www.youtube.com/watch?v=mjQNJ9_gfdI', '2014-08-01 00:56:58'),
(18, 1, 'test@gmail.com', 'https://www.youtube.com/watch?v=kKot9mzcRUE', '2014-07-25 09:38:15'),
(19, 1, 'test@gmail.com', 'https://www.youtube.com/watch?v=GqsE29ij-sA', '2014-07-26 02:33:27'),
(20, 1, 'test@gmail.com', 'https://www.youtube.com/watch?v=1lOlnGG-jtM', '2014-07-26 03:15:16'),
(21, 1, 'nithinredd@gmail.com', 'https://www.youtube.com/watch?v=ENmv-sLuDqY', '2014-07-26 06:54:28'),
(23, 1, 'test1@test.com', 'https://www.icloud.com/download/documents/?p=03&t=BAIOVA6tYPVdFeCmZx4Bi6krnNVfBDzaa80F', '2014-07-27 11:22:38'),
(24, 1, 'test1@test.com', 'https://www.icloud.com/download/documents/?p=03&t=BAIdELF0EQa9AHAYdQgBDFna-QlIKQwgec0F', '2014-07-27 12:19:16'),
(25, 1, 'nithinredd@gmail.com', 'http://youtu.be/5A2ye3iS21U', '2014-07-28 09:02:41'),
(26, 1, 'nithinredd@gmail.com', 'http://youtu.be/XrwsuTzcFh4', '2014-07-28 09:23:31'),
(27, 1, 'nithinredd@gmail.com', 'http://youtu.be/HTWmwfJGQgg', '2014-07-28 09:25:45'),
(33, 1, 'rajeev.uppala@gmail.com', 'http://youtu.be/UPW-kLMO1Y8', '2014-12-16 08:56:28'),
(36, 11, 'rajeev.uppala@gmail.com', 'http://youtu.be/jnM1N_ORVhI', '2015-03-25 07:57:03'),
(37, 11, 'srinivasulu.1216@gmail.com', 'https://www.youtube.com/watch?v=LAlhlub1mj8', '2015-03-25 12:46:43'),
(38, 11, 'rajeev.uppala@gmail.com', 'https://www.youtube.com/watch?v=vSrCUThh4Gc', '2015-08-10 05:50:22'),
(39, 166, 'aman@webappclouds.com', 'http://youtu.be/BEuk7OihPlI', '2015-11-25 00:43:08'),
(40, 158, 'kranthiethan@gmail.com', 'http://youtu.be/gOWtPuvNpYM', '2016-02-25 08:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `teamdocuments`
--

CREATE TABLE IF NOT EXISTS `teamdocuments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teammember_id` int(11) NOT NULL,
  `salon_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `documentname` varchar(250) NOT NULL,
  `documenturl` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `teamdocuments`
--

INSERT INTO `teamdocuments` (`id`, `teammember_id`, `salon_id`, `module_id`, `documentname`, `documenturl`, `created`) VALUES
(23, 71, 1, 1, 'doc1', 'http://Sportyfox.s3.amazonaws.com/1_doc1_googleplay.png', '2014-08-07 01:22:40'),
(24, 71, 1, 1, 'doc2', 'http://Sportyfox.s3.amazonaws.com/1_doc2_linkedin.png', '2014-08-07 01:22:40');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
